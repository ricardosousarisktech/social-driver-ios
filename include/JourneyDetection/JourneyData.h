//
//  JourneyData.h
//
//  Created by Gareth Carter on 24/06/2015.
//  Copyright © 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JourneyData : NSObject <NSCoding>

/**
 * Timestamp (UTC) yyyy-MM-ddTHH:mm:ss.SSSZ
 */
@property (nonatomic) NSString *locationTimestamp;

/**
 * String event name
 * "Start" / "In Progress" / "End" / "Acceleration"
 */
@property (nonatomic) NSString *event;

/**
 * Message Type
 * Timed = 1 / JourneyStart = 3 / JourneyEnd = 4
 * AccelThresholdExceeded = 51
 * DecelThresholdExceeded = 52
 */
@property (nonatomic) NSString *msgType;

/**
 * Latitude in degrees
 */
@property (nonatomic) NSString *lat;

/**
 * Longitude in degrees
 */
@property (nonatomic) NSString *lon;

/**
 * Accuracy in metres
 */
@property (nonatomic) NSString *gpsAccuracy;

/**
 * Speed in km/h
 */
@property (nonatomic) NSString *speed;

/**
 * Activity
 * This is always "Vehicle"
 */
@property (nonatomic) NSString *activity;

/**
 * Battery level 0.0 - 1.0 (fully charged)
 */
@property (nonatomic) NSString *battery;

/**
 * Heading in degrees
 */
@property (nonatomic) NSString *heading;

@end

