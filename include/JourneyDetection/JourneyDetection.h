//
//  JourneyDetection.h
//  JourneyDetection
//
//  Created by Gareth Carter on 17/07/2015.
//  Copyright © 2015 Risk. All rights reserved.
//
//  Main Header file for journey detection library
//  Using this libJourneyDetection.a you can start the journey detection


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface JourneyDetection : NSObject


/**
 Must be called to start the GPS task that will monitor if you are driving or not.
 This will collect GPS data every 60s (10s worth) and see if the speed is above

 @return 1 if ok, 0 error.
 */
- (int) startMonitoring;


/**
 Will stop the Location Manager from collecting GPS data by turning the GPS off for the App.
 
 @return 1 if ok, 0 error.
 */
- (int) stopMonitoring;

/**
 This is used if the user wants to track a journey, GPS data is collected every 60s.
 The only way to stop data collection is to call stopJourney.
 
 @return 1 if ok, 0 error.
 */
- (int) startJourney;

/**
 This is for the user to end the journey collection.
 
 @return 1 if ok, 0 error.
 */
- (int) stopJourney;

/**
 Journey ended Callback Block pass the array of journeys and then deletes them (see JourneyData.h for structure of data)
 
 @param1 journey NSMutableArray of journey data
 
 */
@property (copy) void (^journeyCompleteBlock) (NSMutableArray *journey);

/**
 Read all the current journeys. This will return all the journey data collected.
 
 @return NSMutableArray of all the journeys see JourneyData.h for the type
 */
- (NSMutableArray *) getAllJourneys;

/**
 Delete all the journeys.
 
 @return 1 if ok, 0 error.
 */
- (int) deleteAllJourneys;

/**
 Set Debug Log on/off (off by default)
 @param1 set true enabling logging, false disable logging
 
 @return 1 if ok, 0 error.
 */
- (int) debugLog:(bool)set;

/**
 Get the debug logfile data
 
 @return NSData of the logfile
 */
- (NSData*) debugGetData;

/**
 Clear the logfile
 
 @return 1 if ok, 0 error
 */
- (int) debugClear;

@end
