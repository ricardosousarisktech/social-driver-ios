//
//  AppDelegate.h
//  socialdriver
//
//  Created by Ricardo on 20/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Google/CloudMessaging.h>
#import <UIKit/UIKit.h>

@class MVYSideMenuOptions;

@interface AppDelegate : UIResponder <UIApplicationDelegate, GGLInstanceIDDelegate, GCMReceiverDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, readonly, strong) NSString *registrationKey;
@property(nonatomic, readonly, strong) NSString *messageKey;
@property(nonatomic, readonly, strong) NSString *gcmSenderID;
@property(nonatomic, readonly, strong) NSDictionary *registrationOptions;

+ (AppDelegate *)staticInstance;
- (MVYSideMenuOptions *)sideMenuOptions;

@end

