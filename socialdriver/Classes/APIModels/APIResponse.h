//
//  APIResponse.h
//  socialdriver
//
//  Created by Ricardo on 20/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIResponse : NSObject
@property NSString *success;
@property NSNumber *errorCode;
@property NSString *errorDescription;
@end
