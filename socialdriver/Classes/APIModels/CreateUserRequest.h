//
//  CreateUserRequest.h
//  socialdriver
//
//  Created by Ricardo on 17/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "BaseModel.h"

@interface CreateUserRequest : BaseModel
@property NSNumber *appId;
@property NSString *oAuthid;
@property NSString *username;
@property NSString *password;
@property NSString *timezone;
@property NSString *culture;

- (instancetype)initWithOAuthid: (NSString*)OAuthid AppId: (NSNumber*)AppId Timezone: (NSString*)Timezone Culture: (NSString*)Culture;
- (instancetype)initWithUsername: (NSString*)Username Password: (NSString*)Password AppId: (NSNumber*)AppId Timezone: (NSString*)Timezone Culture: (NSString*)Culture;
@end
