//
//  CreateUserRequest.m
//  socialdriver
//
//  Created by Ricardo on 17/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "CreateUserRequest.h"

@implementation CreateUserRequest
- (instancetype)initWithOAuthid: (NSString*)OAuthid AppId:(NSNumber*)AppId Timezone: (NSString*)Timezone Culture: (NSString*)Culture {
    self.appId = AppId;
    self.oAuthid = OAuthid;
    self.timezone = Timezone;
    self.culture = Culture;
    
    return self;
}
- (instancetype)initWithUsername: (NSString*)Username Password: (NSString*)Password AppId: (NSNumber*)AppId Timezone: (NSString*)Timezone Culture: (NSString*)Culture {
    self.appId = AppId;
    self.username = Username;
    self.password = Password;
    self.timezone = Timezone;
    self.culture = Culture;
    
    return self;
}
@end
