//
//  LoginResponse.h
//  socialdriver
//
//  Created by Ricardo on 21/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "APIResponse.h"

@interface LoginResponse : APIResponse
@property NSNumber *userId;
@property NSString *secret;
@property NSString *userSocialDriver;
@end
