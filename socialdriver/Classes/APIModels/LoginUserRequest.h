//
//  LoginUserRequest.h
//  socialdriver
//
//  Created by Ricardo on 18/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginUserRequest : NSObject
@property NSString *username;
@property NSString *password;
@property NSString *oAuthId;
@property NSNumber *appID;

- (id) initWithUsername: (NSString*)username password: (NSString*)password appId: (NSNumber*)appId;
- (id) initWithOAuthId: (NSString*)oAuthId appId: (NSNumber*)appId;
@end
