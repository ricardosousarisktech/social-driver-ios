//
//  LoginUserRequest.m
//  socialdriver
//
//  Created by Ricardo on 18/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "LoginUserRequest.h"

@implementation LoginUserRequest
- (id) initWithUsername: (NSString*)username password: (NSString*)password appId: (NSNumber*)appId {
    self.username = username;
    self.password = password;
    self.appID = appId;
    
    return self;
}

- (id) initWithOAuthId: (NSString*)oAuthId appId: (NSNumber*)appId {
    self.oAuthId = oAuthId;
    self.appID = appId;
    
    return self;
}
@end
