//
//  ProcessJourneyRequest.h
//  socialdriver
//
//  Created by Ricardo on 20/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MobileTelemetry.h"

@interface ProcessJourneyRequest : NSObject
@property NSNumber *userid;
@property MobileTelemetry *mobileTelemetry;
@property NSNumber *minsToTrim;

- (instancetype)initWithValues: (NSNumber*)userId telemetry: (MobileTelemetry*)telemetry minsToTrim: (NSNumber*)trim;
@end
