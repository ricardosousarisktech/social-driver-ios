//
//  ProcessJourneyRequest.m
//  socialdriver
//
//  Created by Ricardo on 20/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "ProcessJourneyRequest.h"

@implementation ProcessJourneyRequest
- (instancetype)initWithValues: (NSNumber*)userId telemetry: (MobileTelemetry*)telemetry minsToTrim: (NSNumber*)trim {
    self.userid = userId;
    self.mobileTelemetry = telemetry;
    self.minsToTrim = trim;
    
    return self;
}
@end
