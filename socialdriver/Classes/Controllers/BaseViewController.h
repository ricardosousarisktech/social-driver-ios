//
//  BaseViewController.h
//  socialdriver
//
//  Created by Ricardo on 21/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (void)changeViewController:(UIViewController *)viewController;

@property (strong, nonatomic) UIView *pickerParentView;
@property (nonatomic, assign) CGFloat screenWidth;
@property (nonatomic, assign) CGFloat screenHeight;
@property (nonatomic, assign) CGFloat pickerHeight;
@property (nonatomic, assign) CGFloat pickerViewYpositionHidden;
@property (nonatomic, assign) CGFloat pickerViewYposition;

- (void)addNavigationBar:(NSString *)title withBurgerMenu:(BOOL)flag;
- (IBAction)openMenu:(id)sender;
@end
