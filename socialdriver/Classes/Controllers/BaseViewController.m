//
//  BaseViewController.m
//  socialdriver
//
//  Created by Ricardo on 21/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"
#import "MVYSideMenuController.h"
#import "UIImage+Tint.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
        
    [self configureHelperSizes];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)changeViewController:(UIViewController *)viewController
{
    UIViewController *nextViewController = viewController;
    UIViewController *currentViewController = self.sideMenuController.contentViewController;
    
    UIViewController *currentCheckController = ([currentViewController isKindOfClass:[UINavigationController class]] ? [(UINavigationController *)currentViewController topViewController] : currentViewController);
    UIViewController *nextCheckController = ([nextViewController isKindOfClass:[UINavigationController class]] ? [(UINavigationController *)nextViewController topViewController] : nextViewController);
    
    if([nextCheckController isKindOfClass:[currentCheckController class]])
    {
        [self.sideMenuController closeMenu];
    }
    else
    {
        [self.sideMenuController changeContentViewController:viewController closeMenu:YES];
    }
}

- (void)configureHelperSizes {
    self.screenWidth = [[UIScreen mainScreen] bounds].size.width;
    self.screenHeight = [[UIScreen mainScreen] bounds].size.height;
    self.pickerHeight = self.screenHeight / 3;
    self.pickerViewYpositionHidden = self.screenHeight + self.pickerHeight;
    self.pickerViewYposition = self.screenHeight - self.pickerHeight;
    [self.pickerParentView setFrame:CGRectMake(0, self.pickerViewYpositionHidden, self.view.frame.size.width, self.view.frame.size.height)];
}

- (void)addNavigationBar:(NSString *)title withBurgerMenu:(BOOL)flag{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationItem setTitle:title];
    if (flag) {
        UIImage *burgerMenuImage = [[UIImage imageNamed:@"burger-menu-icon"] imageTintedWithColor:[UIColor whiteColor]];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:burgerMenuImage
                                                                                 style:UIBarButtonItemStylePlain target:self action:@selector(openMenu:)];
    }
}

- (IBAction)openMenu:(id)sender {
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    if (sideMenuController) {
        [sideMenuController openMenu];
    }
}
@end