//
//  BluetoothSelectionViewController.h
//  socialdriver
//
//  Created by Ricardo on 11/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "BaseViewController.h"
#import <ExternalAccessory/ExternalAccessory.h>

@interface BluetoothSelectionViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *bluetoothDevicesTableView;

@property (strong, nonatomic) NSMutableArray *accessoryList;

// USE TO SCAN FOR BLUETOOTH LE
//@property (nonatomic, strong) CBCentralManager *centralManager;

@end
