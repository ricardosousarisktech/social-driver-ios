//
//  BluetoothSelectionViewController.m
//  socialdriver
//
//  Created by Ricardo on 11/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "BluetoothSelectionViewController.h"
#import <ExternalAccessory/ExternalAccessory.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "StoryboardIdentifiers.h"
#import "Helpers.h"
#import "BTDevice.h"
#import "MultiSelectionTableViewCell.h"
#import "UIColor+SocialDriver.h"
#import "UIImage+Tint.h"

typedef enum
{
    TableSectionKnownBluetooth,
    TableSectionJourneyStartBluetooth,
    TableSectionCount,
}TableSections;

@interface BluetoothSelectionViewController () <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSMutableArray *journeyStartAcessoryList;
@property (strong, nonatomic) UIBarButtonItem *editButton;
@property (strong, nonatomic) UIBarButtonItem *swapButton;
@property (strong, nonatomic) UIBarButtonItem *cancelButton;
@property BOOL selectableItems;
@end

@implementation BluetoothSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [super addNavigationBar:@"Bluetooth" withBurgerMenu:NO];
    
    [Helpers scanForBluetoothDevices];
    self.accessoryList = (NSMutableArray *)[Helpers loadObjectWithName:@"bluetooth"];
    self.journeyStartAcessoryList = (NSMutableArray *)[Helpers loadObjectWithName:@"bluetooth-start"];
    if (!self.journeyStartAcessoryList) {
        self.journeyStartAcessoryList = [[NSMutableArray alloc] init];
    }
    
    [self.bluetoothDevicesTableView registerNib:[UINib nibWithNibName:@"MultiSelectionTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:IdentTVCMultiSelection];
    self.bluetoothDevicesTableView.sectionHeaderHeight = 60.0;
    
    self.editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(showItemsSelector)];
    self.swapButton = [[UIBarButtonItem alloc] initWithTitle:@"Swap" style:UIBarButtonItemStylePlain target:self action:@selector(moveTableCells)];
    self.cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(hideItemsSelector)];
    self.navigationItem.rightBarButtonItem = self.editButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark – UITableViewDataSource

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *holderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60.0)];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:holderView.bounds];
    [titleLabel setText:[self tableView:tableView titleForHeaderInSection:section]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setFont:[UIFont fontWithName:@"Helvetica-NeueLight" size:20.0]];
    [titleLabel setTextColor:[UIColor settingsGreyFontColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    [holderView addSubview:titleLabel];
    
    return holderView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return TableSectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == TableSectionKnownBluetooth) {
        return self.accessoryList.count;
    } else if (section == TableSectionJourneyStartBluetooth) {
        return self.journeyStartAcessoryList.count;
    }
    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == TableSectionKnownBluetooth) {
        return @"Known devices";
    } else if (section == TableSectionJourneyStartBluetooth) {
        return @"Journey start devices";
    }
    
    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MultiSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IdentTVCMultiSelection];
    NSUInteger row = [indexPath row];
    if (indexPath.section == TableSectionKnownBluetooth) {
        BTDevice *cellDevice = (BTDevice*) [self.accessoryList objectAtIndex:row];
        [cell.descriptionLabel setText:cellDevice.deviceName];
    } else if (indexPath.section == TableSectionJourneyStartBluetooth) {
        BTDevice *cellDevice = (BTDevice*) [self.journeyStartAcessoryList objectAtIndex:row];
        [cell.descriptionLabel setText:cellDevice.deviceName];
    }
    
    if (cell.selected) {
        cell.selectorView.backgroundColor = [UIColor titleBackgroundBlueColor];
    } else {
        cell.selectorView.backgroundColor = [UIColor clearColor];
    }
    
    return cell;
}

#pragma mark – UITableViewDelegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MultiSelectionTableViewCell *cell = (MultiSelectionTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.section == TableSectionKnownBluetooth) {
        cell.selectorView.backgroundColor = [UIColor titleBackgroundBlueColor];
        UIImageView *tick =  (UIImageView *)[cell.selectorView viewWithTag:101];
        if(!tick) {
            tick = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"tick-icon"] imageTintedWithColor:[UIColor whiteColor]]];
            [cell.selectorView addSubview:tick];
            [tick setTag:101];
            tick.frame = cell.selectorView.bounds;
            [tick setContentMode:UIViewContentModeCenter];
        }
        else {
            [tick setImage:[[UIImage imageNamed:@"tick-icon"] imageTintedWithColor:[UIColor whiteColor]]];
        }
        
        for (NSIndexPath *indexPath in [tableView indexPathsForSelectedRows]) {
            if (indexPath.section == TableSectionJourneyStartBluetooth) {
                self.navigationItem.rightBarButtonItem = self.swapButton;
                return;
            }
        }
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStylePlain target:self action:@selector(moveTableCells)];
    } else if (indexPath.section == TableSectionJourneyStartBluetooth) {
        cell.selectorView.backgroundColor = [UIColor badRedColor];
        
        UIImageView *line =  (UIImageView *)[cell.selectorView viewWithTag:101];
        if(!line) {
            line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"white-line-asset"]];
            [cell.selectorView addSubview:line];
            [line setTag:101];
            line.frame = cell.selectorView.bounds;
            [line setContentMode:UIViewContentModeCenter];
        }
        else {
            [line setImage:[UIImage imageNamed:@"white-line-asset"]];
        }
        
        for (NSIndexPath *indexPath in [tableView indexPathsForSelectedRows]) {
            if (indexPath.section == TableSectionKnownBluetooth) {
                self.navigationItem.rightBarButtonItem = self.swapButton;
                return;
            }
        }
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Remove" style:UIBarButtonItemStylePlain target:self action:@selector(moveTableCells)];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectableItems) {
        return indexPath;
    } else {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *paths = [tableView indexPathsForSelectedRows];
    if (!paths) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    MultiSelectionTableViewCell *cell = (MultiSelectionTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [[cell.selectorView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    cell.selectorView.backgroundColor = [UIColor clearColor];
}

- (void)showItemsSelector {
    self.selectableItems = YES;
    self.navigationItem.rightBarButtonItem = self.cancelButton;
    [self makeSelectorViewsAppear:self.bluetoothDevicesTableView];
}

- (void)hideItemsSelector {
    self.selectableItems = NO;
    self.navigationItem.rightBarButtonItem = self.editButton;
    [self makeSelectorViewsDisappear:self.bluetoothDevicesTableView];
}

- (void)moveTableCells {
    NSArray *paths = [self.bluetoothDevicesTableView indexPathsForSelectedRows];
    
    NSMutableArray *knownDevicesToMove = [NSMutableArray array];
    NSMutableArray *startDevicesToMove = [NSMutableArray array];
    
    for(NSIndexPath* indexPath in paths)
    {
        if(indexPath.section == TableSectionKnownBluetooth)
        {
            [knownDevicesToMove addObject:self.accessoryList[indexPath.row]];
        }
        else if(indexPath.section == TableSectionJourneyStartBluetooth)
        {
            [startDevicesToMove addObject:self.journeyStartAcessoryList[indexPath.row]];
        }
    }
    
    [self.accessoryList removeObjectsInArray:knownDevicesToMove];
    [self.journeyStartAcessoryList removeObjectsInArray:startDevicesToMove];
    
    [self.accessoryList addObjectsFromArray:startDevicesToMove];
    [self.journeyStartAcessoryList addObjectsFromArray:knownDevicesToMove];
    
    [self.bluetoothDevicesTableView reloadData];
    
    [self makeSelectorViewsDisappear:self.bluetoothDevicesTableView];
    
    self.navigationItem.rightBarButtonItem = self.editButton;
    self.selectableItems = NO;
}

- (void)makeSelectorViewsAppear:(UITableView *)table {
    for (NSIndexPath *indexPath in [table indexPathsForVisibleRows]) {
        [table deselectRowAtIndexPath:indexPath animated:NO];
        MultiSelectionTableViewCell *cell = (MultiSelectionTableViewCell *) [table cellForRowAtIndexPath:indexPath];
        [UIView animateWithDuration:0.3 animations:^{
            cell.selectorView.transform = CGAffineTransformMakeTranslation(cell.selectorView.layer.anchorPoint.x, 0);
            cell.descriptionLabel.transform = CGAffineTransformMakeTranslation(cell.selectorView.layer.anchorPoint.y, 0);
        }];
    }
}

- (void)makeSelectorViewsDisappear:(UITableView *)table {
    for (NSIndexPath *indexPath in [table indexPathsForVisibleRows]) {
        [table deselectRowAtIndexPath:indexPath animated:NO];
        MultiSelectionTableViewCell *cell = (MultiSelectionTableViewCell *) [table cellForRowAtIndexPath:indexPath];
        [UIView animateWithDuration:0.3 animations:^{
            cell.selectorView.transform = CGAffineTransformMakeTranslation(cell.selectorView.layer.anchorPoint.x - 50, 0);
            cell.descriptionLabel.transform = CGAffineTransformMakeTranslation(cell.descriptionLabel.layer.anchorPoint.x - 50, 0);
        }];
    }
}


- (void)audioRouteHasChangedNotification:(NSNotification*)notification
{
    [self.accessoryList removeAllObjects];
}

- (void)dealloc {
    [Helpers saveObject:self.accessoryList toDiskWithName:@"bluetooth"];
    [Helpers saveObject:self.journeyStartAcessoryList toDiskWithName:@"bluetooth-start"];
}
@end
