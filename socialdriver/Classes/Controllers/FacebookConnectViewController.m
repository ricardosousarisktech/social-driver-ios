//
//  FacebookConnectViewController.m
//  socialdriver
//
//  Created by Ricardo on 21/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

@import CoreLocation;
#import <Foundation/Foundation.h>
#import "FacebookConnectViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <RestKit/RestKit.h>
#import "StoryboardIdentifiers.h"
#import "MVYMenuViewController.h"
#import "HomeViewController.h"
#import "MVYSideMenuController.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "Helpers.h"
#import "CreateUserRequest.h"
#import "LoginUserRequest.h"
#import "RestAPIRequest.h"
#import "LoginResponse.h"

@interface FacebookConnectViewController () <FBSDKLoginButtonDelegate>
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *fbLoginButton;
@property (strong, nonatomic) CLLocationManager *localionManager;
@property NSString *facebookID;
@end

@implementation FacebookConnectViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.localionManager = [[CLLocationManager alloc] init];
    
    // If the user hasn't been asked to activate location service yet
    if (CLLocationManager.authorizationStatus == kCLAuthorizationStatusNotDetermined) {
        [self.localionManager requestAlwaysAuthorization];
    }
    
    self.fbLoginButton.readPermissions = @[@"user_birthday", @"user_hometown", @"user_location"];
}


- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error
{
    // Request a login to RTL:
    if (!error) {
        self.facebookID = result.token.userID;
        LoginUserRequest *userRequest = [[LoginUserRequest alloc]initWithOAuthId:self.facebookID appId:@2];
        [self loginUser: [RestAPIRequest loginUserWithFacebook] userRequest:userRequest];
    } else {
        [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:@"Something went wrong, please try again..."];
    }
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
}

- (void)loginUser: (RKObjectManager*) objectManager userRequest: (LoginUserRequest*)userRequest {
    [objectManager postObject:userRequest path:LoginURL parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        LoginResponse *result = [mappingResult firstObject];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if ([result.success isEqualToString:@"false"] && [result.errorCode isEqualToNumber:@700]) {
            // FacebookID not found on database, create a new user:
            [self createUser];
        } else if ([result.success isEqualToString:@"false"] && ![result.errorCode isEqualToNumber:@700]) {
            [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:result.errorDescription];
        } else if ([result.success isEqualToString:@"true"]) {
            [Helpers saveUserCredentials:[result.userId stringValue] secret:result.secret];
            [self initHomeViewController];            
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:@"Something went wrong, check your network connection and try again..."];
    }];
}

- (void)createUser {
    RKObjectManager *objectCreateUser = [RestAPIRequest createUserUsingFacebook];
    CreateUserRequest *fbUser = [[CreateUserRequest alloc] initWithOAuthid:self.facebookID AppId:@2 Timezone:[Helpers currentTimezone] Culture:[Helpers deviceLanguage]];
    [objectCreateUser postObject:fbUser path:CreateUserURL parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        LoginResponse *result = [mappingResult firstObject];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if ([result.success isEqualToString:@"true"]) {
            [Helpers saveUserCredentials:[result.userId stringValue] secret:result.secret];
            [self initHomeViewController];
        } else {
            [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:result.errorDescription];            
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:@"Something went wrong, please try again..."];
    }];
}

- (void)initHomeViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:StoryboardDashboardFlow bundle:[NSBundle mainBundle]];
    MVYMenuViewController *menuVC = [storyboard instantiateViewControllerWithIdentifier:IdentVCDashboardMenu];
    HomeViewController *contentVC = [storyboard instantiateViewControllerWithIdentifier:IdentVCDashboardNav];
    MVYSideMenuController *sideMenuController = [[MVYSideMenuController alloc] initWithMenuViewController:menuVC contentViewController:contentVC options:[[AppDelegate staticInstance] sideMenuOptions]];
    // Change the menu's width and height:
    sideMenuController.menuFrame = CGRectMake(0, 0, (self.navigationController.view.bounds.size.width * 0.7), self.navigationController.view.bounds.size.height);
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

- (void)testRestRequest {
    NSString *currentTZ = [NSTimeZone systemTimeZone].name;
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLog(@"%@ and %@", currentTZ, language);
}

@end
