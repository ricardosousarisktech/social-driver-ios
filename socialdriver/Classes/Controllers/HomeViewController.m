//
//  HomeViewController.m
//  socialdriver
//
//  Created by Ricardo on 22/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

@import CoreLocation;
#import <Foundation/Foundation.h>
#import "HomeViewController.h"
#import "StoryboardIdentifiers.h"
#import "MVYSideMenuController.h"
#import "MVYMenuViewController.h"
#import "TheWheel.h"
#import "AppDelegate.h"
#import "UIColor+SocialDriver.h"
#import "JourneyDetection.h"
#import "JourneyData.h"
#import "UIImage+Tint.h"
#import <RestKit/RestKit.h>
#import "SDJourneyDetection.h"
#import "Helpers.h"
#import "MobileTelemetry.h"
#import "RestAPIRequest.h"
#import "ProcessJourneyRequest.h"
#import "Constant.h"
#import "RKObjectManager+PutQueryParams.h"

@interface HomeViewController () <UIAlertViewDelegate, NSURLConnectionDataDelegate>
@property (weak, nonatomic) IBOutlet UIButton *burgerMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *selectedMonth;
@property (weak, nonatomic) IBOutlet UIView *scoreCircleView;
@property (weak, nonatomic) IBOutlet UIView *theWheelView;
@property (weak, nonatomic) IBOutlet UILabel *demographicLabel;
@property (weak, nonatomic) IBOutlet UIView *journeyDetectionView;
@property (weak, nonatomic) IBOutlet UIButton *journeyDetectionButton;
@property (weak, nonatomic) IBOutlet UILabel *yourJourneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *journeyDurationLabel;

@property (strong, nonatomic) UIPickerView *monthPicker;
@property (strong, nonatomic) NSArray* pickerArray;

@property (strong, nonatomic) NSMutableData *receivedData;
@property (strong, nonatomic) NSTimer *repeatTimer;
@end

// Prevent first change to demographic label to be animated:
BOOL animateDemographicLabel = NO;
BOOL journeyStarted = NO;

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[SDJourneyDetection sharedJourneyDetection] debugLog:YES];
    
    [self.burgerMenuButton setImage:[[self.burgerMenuButton imageForState:UIControlStateNormal] imageTintedWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    self.demographicLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0];
    self.demographicLabel.textColor = [UIColor whiteColor];
    self.demographicLabel.textAlignment = NSTextAlignmentCenter;
    
    [self initPicker];

    [self configScoreCircle];
    
    [self configWheel];
    
    [self configJourneyDetectionButton];
    
    [SDJourneyDetection sharedJourneyDetection].journeyCompleteBlock = ^(NSMutableArray *journeys) {
        self.journeyDurationLabel.hidden = NO;
        self.journeyDurationLabel.text = @"Journey ended automatically!";
    };
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (IBAction)burgerMenuOpen:(id)sender {
    [self openMenu:sender];
}

-(void)configScoreCircle {
    [self.view updateConstraints];
    self.scoreCircleView.layer.cornerRadius = self.scoreCircleView.frame.size.height/2;
    self.scoreCircleView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.scoreCircleView.layer.borderWidth = 1.0;
}

-(void)configJourneyDetectionButton {
    self.journeyDetectionView.frame = CGRectMake(0, 0,
                                                 self.journeyDetectionView.bounds.size.height,
                                                 self.journeyDetectionView.bounds.size.height);
    self.journeyDetectionView.layer.cornerRadius = self.journeyDetectionView.frame.size.height/2;
    self.journeyDetectionView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.journeyDetectionView.layer.borderWidth = 0.7;
    
    self.journeyDetectionButton.layer.cornerRadius = self.journeyDetectionButton.frame.size.height/2;

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"autojourney"]) {
        [self.journeyDetectionButton setBackgroundColor:[UIColor settingsGreyFontColor]];
        [self.journeyDetectionButton setImage:[[self.journeyDetectionButton imageForState:UIControlStateNormal]
                                               imageTintedWithColor:[UIColor greyButtonColor]] forState:UIControlStateNormal];
        [self.journeyDetectionButton setImage:[[self.journeyDetectionButton imageForState:UIControlStateNormal]
                                               imageTintedWithColor:[UIColor greyButtonColor]] forState:UIControlStateHighlighted];
    } else {
        [self.journeyDetectionButton setBackgroundColor:[UIColor segmentOriginalColor]];
        [self.journeyDetectionButton setImage:[[self.journeyDetectionButton imageForState:UIControlStateNormal]
                                               imageTintedWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [self.journeyDetectionButton addTarget:self action:@selector(buttonTouch:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)configWheel {
    [self.theWheelView updateConstraints];
    TheWheel *wheel = [[TheWheel alloc] initWithFrame:CGRectMake(0, 0, self.theWheelView.bounds.size.width, self.theWheelView.bounds.size.height) andDelegate:self];
    wheel.center = CGPointMake(self.theWheelView.bounds.size.width / 2, self.theWheelView.bounds.size.height / 2);
    [wheel setClipsToBounds:NO];
    [self.theWheelView addSubview:wheel];
}

- (void)wheelDidChangeValue:(NSString *) newValue {
    if (animateDemographicLabel)
        [self startFade:self.demographicLabel value:newValue];
    else {
        animateDemographicLabel = YES;
        self.demographicLabel.text = newValue;
    }
}
- (IBAction)changeMonthSelection:(id)sender {
    [self showPicker];
}

-(IBAction)startFade:(UILabel *)sender value:(NSString *) newValue{
    [self.demographicLabel setAlpha:1.0f];
    // fade out
    [UIView animateWithDuration:0.5f animations:^{
        [self.demographicLabel setAlpha:0.0f];
    } completion:^(BOOL finished) {
        // change the value
        self.demographicLabel.text = newValue;
        //fade in
        [UIView animateWithDuration:0.5f animations:^{
            [self.demographicLabel setAlpha:1.0f];
        } completion:nil];
    }];
}
- (IBAction)buttonTouch:(UIButton *)sender {
    if (!journeyStarted) {
        self.yourJourneyLabel.hidden = NO;
        // Start journey timer
        self.journeyDurationLabel.text = [NSString stringWithFormat:@"%02dh%02dm%02ds", 0, 0, 0];
        self.repeatTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:[NSDate date] repeats:YES];
        // If the user refused to allow the location service yet
        if (CLLocationManager.authorizationStatus == kCLAuthorizationStatusDenied) {
            NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location services are off"
                                                                message:message
                                                               delegate:self
                                                      cancelButtonTitle:@"Cancel"
                                                      otherButtonTitles:@"Settings", nil];
            [alertView show];
        } else {
            [sender setImage:[[UIImage imageNamed:@"stop-icon"]
                                                   imageTintedWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
            [self startMonitoringJourney];
        }
    } else {
        self.yourJourneyLabel.hidden = YES;
        [self.repeatTimer invalidate];
        self.journeyDurationLabel.text = @"PRESS PLAY TO START YOUR JOURNEY";
        [sender setImage:[[UIImage imageNamed:@"play-icon"]
                          imageTintedWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        if ([[SDJourneyDetection sharedJourneyDetection] stopJourney] == 1) {
            // Success
        }
        if ([[SDJourneyDetection sharedJourneyDetection] stopMonitoring] == 1) {
            // Success
        }
        NSMutableArray *journeysArray = [SDJourneyDetection sharedJourneyDetection].getAllJourneys;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        NSDateFormatter *signatureFormat = [[NSDateFormatter alloc] init];
        [signatureFormat setDateFormat:@"yyyyMMddHHmmss"];
        NSMutableString *messageSignature = [[NSMutableString alloc] init];
        NSMutableString *bodyJSON = [[NSMutableString alloc] initWithString:
                                     [NSString stringWithFormat:@"{\"UserId\":%@,\"MobileTelemetry\":[",
                                      [[NSUserDefaults standardUserDefaults] stringForKey:@"activeuser"]]];
        int processedJourneys = 0;
        for (JourneyData *journey in journeysArray) {
            processedJourneys++;
            NSDate *journeyDate = [formatter dateFromString:[journey locationTimestamp]];
            long timestamp = (long)[journeyDate timeIntervalSince1970] * 1000l;
            if (processedJourneys == journeysArray.count) {
                // Last event = Journey end event
                [bodyJSON appendString:[NSString stringWithFormat:@"{\"MsgType\": %d,", 4]];
            } else {
                [bodyJSON appendString:[NSString stringWithFormat:@"{\"MsgType\": %d,", [[journey msgType] intValue]]];
            }
            [bodyJSON appendString:[NSString stringWithFormat:@"\"GpsTime\": %@,", [NSString stringWithFormat:@"\"/Date(%ld)/\"", timestamp]]];
            [bodyJSON appendString:[NSString stringWithFormat:@"\"GpsLatitude\": %f,", [[journey lat] doubleValue]]];
            [bodyJSON appendString:[NSString stringWithFormat:@"\"GpsLongitude\": %f,", [[journey lon] doubleValue]]];
            [bodyJSON appendString:[NSString stringWithFormat:@"\"GpsOrientation\": %d,", [[journey heading] intValue]]];
            [bodyJSON appendString:[NSString stringWithFormat:@"\"GpsSpeed\": %f,", [[journey speed] doubleValue]]];
            [bodyJSON appendString:[NSString stringWithFormat:@"\"GpsFix\": %d,", [[journey gpsAccuracy] intValue]]];
            [bodyJSON appendString:[NSString stringWithFormat:@"\"AccelDecel\": %d},", 0]];
        }

        [bodyJSON appendString:@"], \"MinsToTrim\": 0}"];
        // Append userid and UserId to signature message (hard codded minsToTrim for now 24/08/2015)
        NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeuser"];
        [messageSignature appendString:[NSString stringWithFormat:@"%i%i%@%@", 0, (int)journeysArray.count, userId, userId]];
        
        [self sendThisMobileTelemetryWithAF: bodyJSON withSignature: messageSignature];
        
        [[SDJourneyDetection sharedJourneyDetection] deleteAllJourneys];
        
        journeyStarted = NO;
    }
}

- (void)startMonitoringJourney {
    if ([[SDJourneyDetection sharedJourneyDetection] startMonitoring] == 1) {
        // Success
    }
    if ([[SDJourneyDetection sharedJourneyDetection] startJourney] == 1) {
        // Success
    }
    [SDJourneyDetection sharedJourneyDetection].journeyCompleteBlock = ^(NSMutableArray *journey) {
        NSString *outputString = @"";
        for (JourneyData *location in journey) {
            outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"Latitude: %@, Longitude: %@\r\n", [location lat], [location lon]]];
        }
        
        NSLog(@"%@", outputString);
    };
    
    journeyStarted = YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        // Send the user to the Settings for this app
        NSURL *settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:settingsURL];
    }
}

- (void)showPicker {
    if (self.monthPicker.hidden) {
        self.monthPicker.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.6];
        [UIView animateWithDuration:0.5 animations:^{
            self.pickerParentView = [[UIView alloc] initWithFrame:CGRectMake(0, self.pickerViewYposition, self.screenWidth, self.screenHeight / 2)];
        } completion:^(BOOL finished) {
            [self.monthPicker setHidden:NO];
        }];
        
        UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.screenWidth,44)];
        [toolBar setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"DONE"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(closePicker)];
        toolBar.items = @[barButtonDone];
        barButtonDone.tintColor=[UIColor whiteColor];
        [barButtonDone setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIFont fontWithName:@"HelveticaNeue" size:14.0], NSFontAttributeName,
                                               [UIColor whiteColor], NSForegroundColorAttributeName,
                                               nil] forState:UIControlStateNormal];
        
        
        [self.pickerParentView addSubview:self.monthPicker];
        [self.pickerParentView addSubview:toolBar];
        [self.view addSubview:self.pickerParentView];
        self.monthPicker.showsSelectionIndicator = YES;
        self.monthPicker.dataSource = self;
        self.monthPicker.delegate = self;
        [self.monthPicker selectRow:[self.monthPicker selectedRowInComponent:0] inComponent:0 animated:YES];
        self.pickerHeight = self.pickerParentView.frame.size.height;
    } else {
        [self closePicker];
    }
}

- (void)closePicker {
    [UIView animateWithDuration:0.5 animations:^{
        self.pickerParentView.frame = CGRectMake(0, self.pickerViewYpositionHidden, self.pickerParentView.bounds.size.width, self.pickerParentView.bounds.size.height);
    } completion:^(BOOL finished) {
        [self.monthPicker setHidden:YES];
    }];
}

- (void)initPicker {
    self.monthPicker = [[UIPickerView alloc] init];
    [self.monthPicker setHidden:YES];
    // TODO Get the first policy start date and add months up to it.
    self.pickerArray = [[NSArray alloc] initWithObjects:@"JULY 2015", @"JUNE 2015", @"MAY 2015", @"APRIL 2015", @"MARCH 2015", @"FEBRUARY 2015", @"JANUARY 2015", nil];
    
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.pickerArray count];
}

#pragma mark- Picker View Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
    [self.selectedMonth setTitle:[self.pickerArray objectAtIndex:row] forState:UIControlStateNormal];
}
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 104)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"HelveticaNeue" size:18.0];
    label.text = self.pickerArray[row];
    return label;
}

- (void)sendThisMobileTelemetry: (NSMutableArray*) mobileTelemetry withSignature: (NSString*)signature {
    RKObjectManager *objectManager = [RestAPIRequest sendJourneyToProcess:signature];
    NSString *encrypedSignature = [Helpers hmacsha1:signature secret:[[NSUserDefaults standardUserDefaults] stringForKey:@"secret"]];
    ProcessJourneyRequest *requestObject = [[ProcessJourneyRequest alloc]
                                            initWithValues: [NSNumber numberWithInt:[[[NSUserDefaults standardUserDefaults] stringForKey:@"activeuser"] intValue]]
                                            telemetry: [mobileTelemetry copy]
                                            minsToTrim: @0];
    NSDictionary *params = @{
                             @"signature": encrypedSignature,
                             @"userid" : [[NSUserDefaults standardUserDefaults] stringForKey:@"activeuser"]
                             };
    
    [objectManager postObject:requestObject path:ProcessJourney queryParameters:params success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        APIResponse *result = [mappingResult firstObject];
        if ([result.success isEqualToString:@"true"]) {
            [Helpers displayAnOKAlertDialogWithTitle:@"Success" andBody:@"Your journey data was sent to RTL API."];
        } else {
            [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:result.errorDescription];
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:@"Something went wrong, please try again..."];
    }];
}

- (void)sendThisMobileTelemetryWithAF: (NSMutableString*) jsonBody withSignature: (NSString*)signature {
    NSString *encrypedSignature = [Helpers hmacsha1:signature secret:[[NSUserDefaults standardUserDefaults] stringForKey:@"secret"]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@?userid=%@&signature=%@", DevAPIURL,
                                          ProcessJourney, [[NSUserDefaults standardUserDefaults] stringForKey:@"activeuser"], encrypedSignature]]];
    [request setHTTPMethod:@"POST"];
    
    //set headers
    NSString *contentType = @"text/json";
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    //create the body
    NSData *postBody = [NSData data];
    postBody = [jsonBody dataUsingEncoding:NSUTF8StringEncoding];
    
    //post
    [request setHTTPBody:postBody];
    
    self.receivedData = [[NSMutableData alloc] init];
    
    NSURLConnection *connection= [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *responseString = [[NSString alloc] initWithData:self.receivedData
                                                     encoding:NSUTF8StringEncoding];
    NSData *responseData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
    id jsonResponse = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    
    if ([[jsonResponse objectForKey:@"Success"] boolValue]) {
        [Helpers displayAnOKAlertDialogWithTitle:@"Success" andBody:@"Your journey data was sent to RTL API."];
    } else {
        [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:[jsonResponse objectForKey:@"ErrorDescription"]];
    }
    return;
}

- (void)updateCounter:(NSTimer*) timer {
    NSDate *startDate = (NSDate *)[timer userInfo];
    NSTimeInterval timePassed = [[NSDate date] timeIntervalSinceDate:startDate];
    
    // Convert interval in seconds to hours, minutes and seconds
    int hours = timePassed / (60 * 60);
    int minutes = ((int)timePassed % (60 * 60)) / 60;
    int seconds = (((int)timePassed % (60 * 60)) % 60);
    NSString *time = [NSString stringWithFormat:@"%02dh%02dm%02ds", hours, minutes, seconds];
    self.journeyDurationLabel.text = time;
}

- (void)dealloc {}

@end