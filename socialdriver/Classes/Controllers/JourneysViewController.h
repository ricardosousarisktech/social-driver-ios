//
//  JourneysViewController.h
//  socialdriver
//
//  Created by Ricardo on 03/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface JourneysViewController : BaseViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@end
