//
//  JourneysViewController.m
//  socialdriver
//
//  Created by Ricardo on 03/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "JourneysViewController.h"
#import "JourneyTableViewCell.h"
#import "StoryboardIdentifiers.h"
#import "MVYSideMenuController.h"
#import "UIImage+Tint.h"

@interface JourneysViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *burgerMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *selectedMonth;
@property (weak, nonatomic) IBOutlet UIView *overallScoreCircle;
@property (weak, nonatomic) IBOutlet UIView *minScoreCircle;
@property (weak, nonatomic) IBOutlet UIView *maxScoreCircle;
@property (weak, nonatomic) IBOutlet UITableView *journeysTable;
@property (weak, nonatomic) IBOutlet UIImageView *distanceTravelledImage;
@property (weak, nonatomic) IBOutlet UIImageView *timeTravelledImage;

@property (strong, nonatomic) UIPickerView *monthPicker;

@property (strong, nonatomic) NSArray *journeys;
@property (strong, nonatomic) NSArray *journeysPickerArray;
@end

@implementation JourneysViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.burgerMenuButton setImage:[[self.burgerMenuButton imageForState:UIControlStateNormal] imageTintedWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    
    [self initPicker];
    
    [self loadJourneys];
    
    [self circlesConfiguration];
    
    [self configViews];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)circlesConfiguration {
    self.overallScoreCircle.layer.cornerRadius = self.overallScoreCircle.bounds.size.width / 2;
    self.overallScoreCircle.layer.borderColor = [UIColor whiteColor].CGColor;
    self.overallScoreCircle.layer.borderWidth = 1.0;
    
    self.minScoreCircle.layer.cornerRadius = self.minScoreCircle.bounds.size.width / 2;
    
    self.maxScoreCircle.layer.cornerRadius = self.maxScoreCircle.bounds.size.width / 2;
}

- (void)configViews {
    self.distanceTravelledImage.image = [self.distanceTravelledImage.image imageTintedWithColor:[UIColor whiteColor]];
    self.timeTravelledImage.image = [self.timeTravelledImage.image imageTintedWithColor:[UIColor whiteColor]];
}

- (void)loadJourneys {
    self.journeys = [[NSArray alloc] initWithObjects:@"Journey one", @"Journey two", @"Journey three", @"Journey four", @"Journey five", nil];
}

#pragma mark - Table data source and delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.journeys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JourneyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IdentTVCJourney];
    
    cell.journeyNameDummyLabel.text = self.journeys[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Open menu action

- (IBAction)burgerMenuOpen:(id)sender {
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    if (sideMenuController) {
        [sideMenuController openMenu];
    }
}

#pragma mark - Month picker section

- (IBAction)changeMonthSelection:(id)sender {
    [self showPicker];
}

- (void)showPicker {
    if (self.monthPicker.hidden) {
        self.monthPicker.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.6];
        [UIView animateWithDuration:0.5 animations:^{
            self.pickerParentView = [[UIView alloc] initWithFrame:CGRectMake(0, self.pickerViewYposition, self.screenWidth, self.screenHeight / 2)];
        } completion:^(BOOL finished) {
            [self.monthPicker setHidden:NO];
        }];
        
        UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.screenWidth,44)];
        [toolBar setBarStyle:UIBarStyleBlackTranslucent];
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"DONE"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(closePicker)];
        toolBar.items = @[barButtonDone];
        barButtonDone.tintColor=[UIColor whiteColor];
        [barButtonDone setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIFont fontWithName:@"HelveticaNeue" size:14.0], NSFontAttributeName,
                                               [UIColor whiteColor], NSForegroundColorAttributeName,
                                               nil] forState:UIControlStateNormal];
        
        
        [self.pickerParentView addSubview:self.monthPicker];
        [self.pickerParentView addSubview:toolBar];
        [self.view addSubview:self.pickerParentView];
        self.monthPicker.showsSelectionIndicator = YES;
        self.monthPicker.dataSource = self;
        self.monthPicker.delegate = self;
        [self.monthPicker selectRow:[self.monthPicker selectedRowInComponent:0] inComponent:0 animated:YES];
        self.pickerHeight = self.pickerParentView.frame.size.height;
    } else {
        [self closePicker];
    }
}

- (void)closePicker {
    [UIView animateWithDuration:0.5 animations:^{
        self.pickerParentView.frame = CGRectMake(0, self.pickerViewYpositionHidden, self.pickerParentView.bounds.size.width, self.pickerParentView.bounds.size.height);
    } completion:^(BOOL finished) {
        [self.monthPicker setHidden:YES];
    }];
}

- (void)initPicker {
    self.monthPicker = [[UIPickerView alloc] init];
    [self.monthPicker setHidden:YES];
    // TODO Get the first policy start date and add month up to it.
    self.journeysPickerArray = [[NSArray alloc] initWithObjects:@"JULY 2015", @"JUNE 2015", @"MAY 2015", @"APRIL 2015", @"MARCH 2015", @"FEBRUARY 2015", @"JANUARY 2015", nil];
}

#pragma mark - Picker View Data source
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.journeysPickerArray count];
}

#pragma mark- Picker View Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:
(NSInteger)row inComponent:(NSInteger)component{
    [self.selectedMonth setTitle:[self.journeysPickerArray objectAtIndex:row] forState:UIControlStateNormal];
}
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 104)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"HelveticaNeue" size:18.0];
    label.text = self.journeysPickerArray[row];
    return label;
}
@end
