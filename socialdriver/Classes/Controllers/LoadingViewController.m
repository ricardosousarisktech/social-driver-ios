//
//  ViewController.m
//  socialdriver
//
//  Created by Ricardo on 20/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "LoadingViewController.h"

@interface LoadingViewController ()

@end

@implementation LoadingViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    //Change properties across the app:
//    [[UILabel appearance] setTextColor:[UIColor greenColor]];
//    [[UILabel appearanceWhenContainedIn:[UINavigationBar class], nil] setTextColor:[UIColor blueColor]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
