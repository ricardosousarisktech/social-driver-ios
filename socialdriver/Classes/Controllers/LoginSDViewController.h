//
//  LoginSDViewController.h
//  socialdriver
//
//  Created by Ricardo on 22/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface LoginSDViewController : BaseViewController <UITextFieldDelegate>

@end
