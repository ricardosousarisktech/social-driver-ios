//
//  LoginSDViewController.m
//  socialdriver
//
//  Created by Ricardo on 22/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "LoginSDViewController.h"
#import "UIColor+SocialDriver.h"
#import "UIImage+Tint.h"
#import "RKObjectManager.h"
#import "RestAPIRequest.h"
#import "LoginUserRequest.h"
#import "Constant.h"
#import "Helpers.h"
#import "MVYMenuViewController.h"
#import "HomeViewController.h"
#import "MVYSideMenuController.h"
#import "StoryboardIdentifiers.h"
#import "AppDelegate.h"

@interface LoginSDViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

@implementation LoginSDViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    // General control configurations
    [self configControls];
    
    // General layout configurations
    [self configViews];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)backBtnClick:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)configControls {
    // Delegate text field action to this controller:
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    // Set click event on back button:
    [self.backButton addTarget:self action:@selector(backBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton setImage:[[self.backButton imageForState:UIControlStateNormal] imageTintedWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
}

-(void)configViews {
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                 initWithString:@"email address..."
                                                 attributes:@{NSForegroundColorAttributeName: [UIColor textFieldsColor],
                                                              NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:15.0]}];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                 initWithString:@"password..."
                                                 attributes:@{NSForegroundColorAttributeName: [UIColor textFieldsColor],
                                                              NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:15.0]}];
    
    self.emailTextField.textColor = [UIColor textFieldsColor];
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    self.emailTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"email-icon"]];
    self.emailTextField.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.emailTextField.layer.borderWidth = 1.0;
    self.emailTextField.layer.cornerRadius = 5.0;
    
    self.passwordTextField.textColor = [UIColor textFieldsColor];
    self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.passwordTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password-icon"]];
    self.passwordTextField.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.passwordTextField.layer.borderWidth = 1.0;
    self.passwordTextField.layer.cornerRadius = 5.0;
}
- (IBAction)loginButtonAction:(id)sender {
    LoginUserRequest *userRequest = [[LoginUserRequest alloc]initWithUsername:self.emailTextField.text password:self.passwordTextField.text appId:@2];
    RKObjectManager *objectManager = [RestAPIRequest loginUser];
    [objectManager postObject:userRequest path:LoginURL parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        LoginResponse *result = [mappingResult firstObject];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if ([result.success isEqualToString:@"false"]) {
            [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:result.errorDescription];
        } else if ([result.success isEqualToString:@"true"]) {
            [Helpers saveUserCredentials:[result.userId stringValue] secret:result.secret];
            [self initHomeViewController];
        }
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:@"Something went wrong, check your network connection and try again..."];
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIResponder* responder = [textField.superview viewWithTag:nextTag];
    if (responder) {
        [responder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return NO;
}

- (void)initHomeViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:StoryboardDashboardFlow bundle:[NSBundle mainBundle]];
    MVYMenuViewController *menuVC = [storyboard instantiateViewControllerWithIdentifier:IdentVCDashboardMenu];
    HomeViewController *contentVC = [storyboard instantiateViewControllerWithIdentifier:IdentVCDashboardNav];
    MVYSideMenuController *sideMenuController = [[MVYSideMenuController alloc] initWithMenuViewController:menuVC contentViewController:contentVC options:[[AppDelegate staticInstance] sideMenuOptions]];
    // Change the menu's width and height:
    sideMenuController.menuFrame = CGRectMake(0, 0, (self.navigationController.view.bounds.size.width * 0.7), self.navigationController.view.bounds.size.height);
    [self.navigationController pushViewController:sideMenuController animated:YES];
}
@end