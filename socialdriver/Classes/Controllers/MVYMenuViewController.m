//
//  MVYMenuViewController.m
//  socialdriver
//
//  Created by Ricardo on 24/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MVYMenuViewController.h"
#import "Constant.h"
#import "HomeViewController.h"
#import "StoryboardIdentifiers.h"
#import "MVYSideMenuController.h"
#import "MenuTableViewCell.h"
#import "Helpers.h"
#import "AppDelegate.h"
#import "FBSDKLoginManager.h"
#import "JourneysViewController.h"
#import "SettingsViewController.h"
#import "MyInformationViewController.h"
#import "FBSDKProfile.h"
#import "FBSDKProfilePictureView.h"
#import "UIImage+Tint.h"
#import "SDJourneyDetection.h"
#import "JourneyData.h"

typedef enum
{
    MenuItemHome,
    MenuItemJourneys,
    MenuItemSettings,
    MenuItemMyInformation,
    MenuItemLogout,
    MenuItemCount, //move anything below here to hide it from the menu
}MenuItems;

@interface MVYMenuViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet FBSDKProfilePictureView *fbProfilePicture;
@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UILabel *profileAddress;
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@end

@implementation MVYMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    [self configViewsWithUserInfo];
    [self configMenuTable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configMenuTable {
    self.menuTableView.backgroundView = nil;
    self.menuTableView.backgroundColor = [UIColor clearColor];
    
//    self.menuTableView.scrollEnabled = (self.menuTableView.contentSize.height > self.menuTableView.frame.size.height);
    
    //TODO: works for small menus
    self.menuTableView.scrollEnabled = NO;
    CGRect frame = self.menuTableView.frame;
    frame.size.height = self.menuTableView.contentSize.height - 1;
    [self.menuTableView setFrame:frame];
}

- (void) configViewsWithUserInfo {
    [self.fbProfilePicture setPictureMode:FBSDKProfilePictureModeNormal];
    [self.fbProfilePicture setProfileID:[FBSDKProfile currentProfile].userID];
    self.fbProfilePicture.layer.cornerRadius = self.fbProfilePicture.frame.size.width/2;
    self.fbProfilePicture.layer.masksToBounds = YES;
    self.fbProfilePicture.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.fbProfilePicture.clipsToBounds = YES;
    
    self.profileName.text = [NSString stringWithFormat:@"%s %s", [FBSDKProfile currentProfile].firstName.UTF8String, [FBSDKProfile currentProfile].lastName.UTF8String];
    FBSDKGraphRequest *graphRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"location"} HTTPMethod:@"GET"];
    [graphRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            NSDictionary *userData = (NSDictionary *)result;
            self.profileAddress.text = userData[@"location"][@"name"];
        }
    }];
}

#pragma mark – UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return MenuItemCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IdentTVCMenu];
    
    switch ((MenuItems)indexPath.row) {
        case MenuItemHome:
            cell.menuLabel.text = MenuItemHomeTitle;
            [cell.iconImageView setImage:[UIImage imageNamed:MenuItemHomeImage]];
            break;
        case MenuItemJourneys:
            cell.menuLabel.text = MenuItemJourneysTitle;
            [cell.iconImageView setImage:[UIImage imageNamed:MenuItemJourneysImage]];
            break;
        case MenuItemSettings:
            cell.menuLabel.text = MenuItemSettingsTitle;
            [cell.iconImageView setImage:[UIImage imageNamed:MenuItemSettingsImage]];
            break;
        case MenuItemMyInformation:
            cell.menuLabel.text = MenuItemMyInformationTitle;
            [cell.iconImageView setImage:[UIImage imageNamed:MenuItemMyInformationImage]];
            break;
        case MenuItemLogout:
            cell.menuLabel.text = MenuItemLogoutTitle;
            [cell.iconImageView setImage:[UIImage imageNamed:MenuItemLogoutImage]];
            break;
        default:
            break;
    }
    
    cell.iconImageView.image = [cell.iconImageView.image imageTintedWithColor:[UIColor whiteColor]];
    
    return cell;
}

#pragma mark – UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HomeViewController *homeVC = (HomeViewController *)[Helpers getViewControllerForIdentifier:IdentVCDashboard fromStoryboardWithIdentifier:StoryboardDashboardFlow];
    JourneysViewController *journeysVC = (JourneysViewController *)[Helpers getViewControllerForIdentifier:IdentVCJourneys fromStoryboardWithIdentifier:StoryboardDashboardFlow];
    SettingsViewController *settingsVC = (SettingsViewController *)[Helpers getViewControllerForIdentifier:IdentVCSettings fromStoryboardWithIdentifier:StoryboardDashboardFlow];
    MyInformationViewController *informationVC = (MyInformationViewController *)[Helpers getViewControllerForIdentifier:IdentVCMyInfo fromStoryboardWithIdentifier:StoryboardDashboardFlow];
    switch ((MenuItems)indexPath.item) {
        case MenuItemHome:
            [self changeViewController:homeVC];
            break;
        case MenuItemJourneys:
            [self changeViewController:journeysVC];
            break;
        case MenuItemSettings:
            [self changeViewController:[[UINavigationController alloc] initWithRootViewController:settingsVC]];
            break;
        case MenuItemMyInformation:
            [self changeViewController:[[UINavigationController alloc] initWithRootViewController:informationVC]];
            break;
        case MenuItemLogout:
            [self logoutUser];
            break;
        default:
            break;
    }
}

- (void)logoutUser {
    FBSDKLoginManager *fbManager = [[FBSDKLoginManager alloc] init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:StoryboardLoginFlow bundle:[NSBundle mainBundle]];
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:IdentLogin];
    [[AppDelegate staticInstance].window setRootViewController:[[UINavigationController alloc] initWithRootViewController:vc]]; // <-- resets whole stack
    
    [fbManager logOut];
}


@end
