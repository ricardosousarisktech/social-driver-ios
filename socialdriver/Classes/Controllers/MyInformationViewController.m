//
//  MyInformationViewController.m
//  socialdriver
//
//  Created by Ricardo on 05/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "MyInformationViewController.h"
#import "MVYSideMenuController.h"
#import "UIImage+Tint.h"
#import "UIColor+SocialDriver.h"

@interface MyInformationViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *genderIcon;
@property (weak, nonatomic) IBOutlet UIImageView *ageIcon;
@property (weak, nonatomic) IBOutlet UIImageView *locationIcon;
@property (weak, nonatomic) IBOutlet UIImageView *makeIcon;
@property (weak, nonatomic) IBOutlet UIImageView *modelIcon;
@property (weak, nonatomic) IBOutlet UIButton *genderButton;
@property (weak, nonatomic) IBOutlet UIButton *ageButton;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;
@property (weak, nonatomic) IBOutlet UIButton *makeButton;
@property (weak, nonatomic) IBOutlet UIButton *modelButton;

@end

@implementation MyInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addNavigationBar:@"My Information" withBurgerMenu:YES];
    [self configureViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureViews {
    self.genderIcon.image = [self.genderIcon.image imageTintedWithColor:[UIColor titleBackgroundBlueColor]];
    [self.genderButton setImage:[[self.genderButton imageForState:UIControlStateNormal] imageTintedWithColor:[UIColor titleBackgroundBlueColor]] forState:UIControlStateNormal];
    self.ageIcon.image = [self.ageIcon.image imageTintedWithColor:[UIColor titleBackgroundBlueColor]];
    [self.ageButton setImage:[[self.ageButton imageForState:UIControlStateNormal] imageTintedWithColor:[UIColor titleBackgroundBlueColor]] forState:UIControlStateNormal];
    self.locationIcon.image = [self.locationIcon.image imageTintedWithColor:[UIColor titleBackgroundBlueColor]];
    [self.locationButton setImage:[[self.locationButton imageForState:UIControlStateNormal] imageTintedWithColor:[UIColor titleBackgroundBlueColor]] forState:UIControlStateNormal];
    self.makeIcon.image = [self.makeIcon.image imageTintedWithColor:[UIColor titleBackgroundBlueColor]];
    [self.makeButton setImage:[[self.makeButton imageForState:UIControlStateNormal] imageTintedWithColor:[UIColor titleBackgroundBlueColor]] forState:UIControlStateNormal];
    self.modelIcon.image = [self.modelIcon.image imageTintedWithColor:[UIColor titleBackgroundBlueColor]];
    [self.modelButton setImage:[[self.modelButton imageForState:UIControlStateNormal] imageTintedWithColor:[UIColor titleBackgroundBlueColor]] forState:UIControlStateNormal];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openMenu:(id)sender {
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    if (sideMenuController) {
        [sideMenuController openMenu];
    }
}

@end
