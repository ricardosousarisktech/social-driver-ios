//
//  RegisterViewController.m
//  socialdriver
//
//  Created by Ricardo on 24/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "RegisterViewController.h"
#import "UIColor+SocialDriver.h"
#import "UIImage+Tint.h"
#import "Helpers.h"
#import "RestAPIRequest.h"
#import "CreateUserRequest.h"
#import "Constant.h"

@interface RegisterViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@end

@implementation RegisterViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    // General control configurations
    [self configControls];
    
    // General layout configurations
    [self configViews];

}

- (void)backBtnClick:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)configControls {
    // Delegate text field action to this controller:
    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.confirmPasswordTextField.delegate = self;
    
    // Set click event on back button:
    [self.backButton addTarget:self action:@selector(backBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton setImage:[[self.backButton imageForState:UIControlStateNormal] imageTintedWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
}

-(void)configViews {
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                 initWithString:@"email address..."
                                                 attributes:@{NSForegroundColorAttributeName: [UIColor textFieldsColor],
                                                              NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:17.0]}];
    self.passwordTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                    initWithString:@"password..."
                                                    attributes:@{NSForegroundColorAttributeName: [UIColor textFieldsColor],
                                                                 NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:17.0]}];
    self.confirmPasswordTextField.attributedPlaceholder = [[NSAttributedString alloc]
                                                           initWithString:@"confirm password..."
                                                           attributes:@{NSForegroundColorAttributeName: [UIColor textFieldsColor],
                                                                        NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:17.0]}];
    
    self.emailTextField.textColor = [UIColor textFieldsColor];
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    self.emailTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"email-icon"]];
    self.emailTextField.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.emailTextField.layer.borderWidth = 1.0;
    self.emailTextField.layer.cornerRadius = 5.0;
    
    self.passwordTextField.textColor = [UIColor textFieldsColor];
    self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.passwordTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password-icon"]];
    self.passwordTextField.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.passwordTextField.layer.borderWidth = 1.0;
    self.passwordTextField.layer.cornerRadius = 5.0;
    
    self.confirmPasswordTextField.textColor = [UIColor textFieldsColor];
    self.confirmPasswordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.confirmPasswordTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"password-icon"]];
    self.confirmPasswordTextField.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.confirmPasswordTextField.layer.borderWidth = 1.0;
    self.confirmPasswordTextField.layer.cornerRadius = 5.0;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    UIResponder* responder = [textField.superview viewWithTag:nextTag];
    if (responder) {
        [responder becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return NO;
}
- (IBAction)joinSocialDriver:(id)sender {
    if ([self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
        RKObjectManager *objectManager = [RestAPIRequest createSocialDriverUser];
        NSString *currentTZ = [NSTimeZone systemTimeZone].name;
        NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
        
        CreateUserRequest *sdUser = [[CreateUserRequest alloc] initWithUsername:self.emailTextField.text
                                                                       Password:self.passwordTextField.text
                                                                          AppId:[NSNumber numberWithInt:[AppID intValue]]
                                                                       Timezone:currentTZ
                                                                        Culture:language];

        [objectManager postObject:sdUser path:CreateUserURL parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            LoginResponse *result = [mappingResult firstObject];
            if ([result.success isEqualToString:@"true"]) {
                [Helpers displayAnOKAlertDialogWithTitle:@"Notification" andBody:@"Please, check your email to confirm your account on Social Driver"];
            } else {
                [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:[result errorDescription]];
            }
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            NSLog(@"Error: %@", error.localizedFailureReason);
        }];

        self.emailTextField.text = @"";
        self.passwordTextField.text = @"";
        self.confirmPasswordTextField.text = @"";
    } else {
        [Helpers displayAnOKAlertDialogWithTitle:@"Error" andBody:@"Passwords do not match"];
    }
}

@end