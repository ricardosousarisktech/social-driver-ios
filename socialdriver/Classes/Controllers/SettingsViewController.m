//
//  SettingsViewController.m
//  socialdriver
//
//  Created by Ricardo on 05/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "SettingsViewController.h"
#import "UIImage+Tint.h"
#import "UIColor+SocialDriver.h"
#import "MVYSideMenuController.h"
#import "SDJourneyDetection.h"

@interface SettingsViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *locationIcon;
@property (weak, nonatomic) IBOutlet UISwitch *locationSwitch;
@property (weak, nonatomic) IBOutlet UIImageView *autojourneyIcon;
@property (weak, nonatomic) IBOutlet UISwitch *autojourneySwitch;
@property (weak, nonatomic) IBOutlet UIImageView *notificationsIcon;
@property (weak, nonatomic) IBOutlet UISwitch *notificationsSwitch;
@property (weak, nonatomic) IBOutlet UIImageView *wifiIcon;
@property (weak, nonatomic) IBOutlet UIImageView *bluetoothIcon;
@property (weak, nonatomic) IBOutlet UIView *wifiDetectionView;
@property (weak, nonatomic) IBOutlet UIButton *wifiButton;
@property (weak, nonatomic) IBOutlet UIButton *bluetoothButton;
@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addNavigationBar:@"Settings" withBurgerMenu:YES];
    [self configureViews];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)autojourneyDetectionSwitch:(id)sender {
    if ([self.autojourneySwitch isOn]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"autojourney"];
        [[SDJourneyDetection sharedJourneyDetection] startMonitoring];
    } else if (![self.autojourneySwitch isOn]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"autojourney"];
        [[SDJourneyDetection sharedJourneyDetection] stopMonitoring];
    }
}

- (void)configureViews {
    self.locationIcon.image = [self.locationIcon.image imageTintedWithColor:[UIColor titleBackgroundBlueColor]];
    self.autojourneyIcon.image = [self.autojourneyIcon.image imageTintedWithColor:[UIColor titleBackgroundBlueColor]];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"autojourney"]) {
        [self.autojourneySwitch setOn:YES];
    } else {
        [self.autojourneySwitch setOn:NO];
    }
    self.notificationsIcon.image = [self.notificationsIcon.image imageTintedWithColor:[UIColor titleBackgroundBlueColor]];
    self.wifiIcon.image = [self.wifiIcon.image imageTintedWithColor:[UIColor titleBackgroundBlueColor]];
    self.bluetoothIcon.image = [self.bluetoothIcon.image imageTintedWithColor:[UIColor titleBackgroundBlueColor]];
    [self.wifiButton setImage:[[self.wifiButton imageForState:UIControlStateNormal] imageTintedWithColor:[UIColor titleBackgroundBlueColor]] forState:UIControlStateNormal];
    [self.bluetoothButton setImage:[[self.bluetoothButton imageForState:UIControlStateNormal] imageTintedWithColor:[UIColor titleBackgroundBlueColor]] forState:UIControlStateNormal];
}

@end
