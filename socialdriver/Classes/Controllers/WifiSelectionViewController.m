//
//  WifiSelectionViewController.m
//  socialdriver
//
//  Created by Ricardo on 07/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "WifiSelectionViewController.h"
#import "MultiSelectionTableViewCell.h"
#import "StoryboardIdentifiers.h"
#import "UIColor+SocialDriver.h"
#import "Helpers.h"
#import "WifiNetwork.h"
#import "UIImage+Tint.h"

typedef enum
{
    TableSectionKnownNetworks,
    TableSectionJourneyStopNetworks,
    TableSectionCount,
}TableSections;

@interface WifiSelectionViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *wifiNetworksTableView;
@property (strong, nonatomic) NSMutableArray *knownNetworksArray;
@property (strong, nonatomic) NSMutableArray *journeyStopNetworksArray;
@property (strong, nonatomic) UIBarButtonItem *editButton;
@property (strong, nonatomic) UIBarButtonItem *swapButton;
@property (strong, nonatomic) UIBarButtonItem *cancelButton;
@property BOOL selectableItems;
@end

@implementation WifiSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [super addNavigationBar:@"Wifi" withBurgerMenu:NO];
    self.editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(showItemsSelector)];
    self.swapButton = [[UIBarButtonItem alloc] initWithTitle:@"Swap" style:UIBarButtonItemStylePlain target:self action:@selector(moveTableCells)];
    self.cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(hideItemsSelector)];
    self.navigationItem.rightBarButtonItem = self.editButton;
    [self.wifiNetworksTableView registerNib:[UINib nibWithNibName:@"MultiSelectionTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:IdentTVCMultiSelection];
    self.wifiNetworksTableView.sectionHeaderHeight = 60.0;
    
    self.knownNetworksArray = (NSMutableArray *)[Helpers loadObjectWithName:@"wifi"];
    self.journeyStopNetworksArray = (NSMutableArray *)[Helpers loadObjectWithName:@"wifi-stop"];
    if (!self.journeyStopNetworksArray) {
        self.journeyStopNetworksArray = [[NSMutableArray alloc] init];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark – UITableViewDataSource

 - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return TableSectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == TableSectionKnownNetworks) {
        return self.knownNetworksArray.count;
    } else if(section == TableSectionJourneyStopNetworks) {
        return self.journeyStopNetworksArray.count;
    }
    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section == TableSectionKnownNetworks)
    {
        return @"Known networks";
    }
    else if(section == TableSectionJourneyStopNetworks)
    {
        return @"Journey stop networks";
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *holderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60.0)];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:holderView.bounds];
    [titleLabel setText:[self tableView:tableView titleForHeaderInSection:section]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setFont:[UIFont fontWithName:@"Helvetica-NeueLight" size:20.0]];
    [titleLabel setTextColor:[UIColor settingsGreyFontColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    [holderView addSubview:titleLabel];
    
    return holderView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MultiSelectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IdentTVCMultiSelection];
    if (indexPath.section == TableSectionKnownNetworks) {
        cell.descriptionLabel.text = [NSString stringWithFormat:@"%@", ((WifiNetwork*)self.knownNetworksArray[indexPath.row]).ssid];
    } else if (indexPath.section == TableSectionJourneyStopNetworks) {
        cell.descriptionLabel.text = [NSString stringWithFormat:@"%@", ((WifiNetwork*)self.journeyStopNetworksArray[indexPath.row]).ssid];
    }
    if (cell.selected) {
        cell.selectorView.backgroundColor = [UIColor titleBackgroundBlueColor];
    } else {
        cell.selectorView.backgroundColor = [UIColor clearColor];
    }
    return cell;
}

#pragma mark – UITableViewDelegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MultiSelectionTableViewCell *cell = (MultiSelectionTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.section == TableSectionKnownNetworks) {
        cell.selectorView.backgroundColor = [UIColor titleBackgroundBlueColor];
        UIImageView *tick =  (UIImageView *)[cell.selectorView viewWithTag:101];
        if(!tick) {
            tick = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"tick-icon"] imageTintedWithColor:[UIColor whiteColor]]];
            [cell.selectorView addSubview:tick];
            [tick setTag:101];
            tick.frame = cell.selectorView.bounds;
            [tick setContentMode:UIViewContentModeCenter];
        } else {
            [tick setImage:[[UIImage imageNamed:@"tick-icon"] imageTintedWithColor:[UIColor whiteColor]]];
        }
        
        for (NSIndexPath *indexPath in [tableView indexPathsForSelectedRows]) {
            if (indexPath.section == TableSectionJourneyStopNetworks) {
                self.navigationItem.rightBarButtonItem = self.swapButton;
                return;
            }
        }
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStylePlain target:self action:@selector(moveTableCells)];
    } else if (indexPath.section == TableSectionJourneyStopNetworks) {
        cell.selectorView.backgroundColor = [UIColor badRedColor];
        
        UIImageView *line =  (UIImageView *)[cell.selectorView viewWithTag:101];
        if(!line)
        {
            line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"white-line-asset"]];
            [cell.selectorView addSubview:line];
            [line setTag:101];
            line.frame = cell.selectorView.bounds;
            [line setContentMode:UIViewContentModeCenter];
        }
        else
        {
            [line setImage:[UIImage imageNamed:@"white-line-asset"]];
        }
        
        for (NSIndexPath *indexPath in [tableView indexPathsForSelectedRows]) {
            if (indexPath.section == TableSectionKnownNetworks) {
                self.navigationItem.rightBarButtonItem = self.swapButton;
                return;
            }
        }
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Remove" style:UIBarButtonItemStylePlain target:self action:@selector(moveTableCells)];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *paths = [tableView indexPathsForSelectedRows];
    if (!paths) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    MultiSelectionTableViewCell *cell = (MultiSelectionTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [[cell.selectorView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    cell.selectorView.backgroundColor = [UIColor clearColor];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.selectableItems) {
        return indexPath;
    } else {
        return nil;
    }
}

- (void)showItemsSelector {
    self.selectableItems = YES;
    self.navigationItem.rightBarButtonItem = self.cancelButton;
    [self makeSelectorViewsAppear:self.wifiNetworksTableView];
}

- (void)hideItemsSelector {
    self.selectableItems = NO;
    self.navigationItem.rightBarButtonItem = self.editButton;
    [self makeSelectorViewsDisappear:self.wifiNetworksTableView];
}

- (void)moveTableCells {
    NSArray *paths = [self.wifiNetworksTableView indexPathsForSelectedRows];
    
    NSMutableArray *knownNetworksToMove = [NSMutableArray array];
    NSMutableArray *stopNetworksToMove = [NSMutableArray array];
    
    for(NSIndexPath* indexPath in paths)
    {
        if(indexPath.section == TableSectionKnownNetworks)
        {
            [knownNetworksToMove addObject:self.knownNetworksArray[indexPath.row]];
        }
        else if(indexPath.section == TableSectionJourneyStopNetworks)
        {
            [stopNetworksToMove addObject:self.journeyStopNetworksArray[indexPath.row]];
        }
    }
    
    [self.knownNetworksArray removeObjectsInArray:knownNetworksToMove];
    [self.journeyStopNetworksArray removeObjectsInArray:stopNetworksToMove];
    
    [self.knownNetworksArray addObjectsFromArray:stopNetworksToMove];
    [self.journeyStopNetworksArray addObjectsFromArray:knownNetworksToMove];

    [self.wifiNetworksTableView reloadData];
    
    [self makeSelectorViewsDisappear:self.wifiNetworksTableView];
    
    self.navigationItem.rightBarButtonItem = self.editButton;
    self.selectableItems = NO;
}

- (void)makeSelectorViewsAppear:(UITableView *)table {
    for (NSIndexPath *indexPath in [table indexPathsForVisibleRows]) {
        [table deselectRowAtIndexPath:indexPath animated:NO];
        MultiSelectionTableViewCell *cell = (MultiSelectionTableViewCell *) [table cellForRowAtIndexPath:indexPath];
        [UIView animateWithDuration:0.3 animations:^{
            cell.selectorView.transform = CGAffineTransformMakeTranslation(cell.selectorView.layer.anchorPoint.x, 0);
            cell.descriptionLabel.transform = CGAffineTransformMakeTranslation(cell.selectorView.layer.anchorPoint.y, 0);
        }];
    }
}

- (void)makeSelectorViewsDisappear:(UITableView *)table {
    for (NSIndexPath *indexPath in [table indexPathsForVisibleRows]) {
        [table deselectRowAtIndexPath:indexPath animated:NO];
        MultiSelectionTableViewCell *cell = (MultiSelectionTableViewCell *) [table cellForRowAtIndexPath:indexPath];
        [UIView animateWithDuration:0.3 animations:^{
            cell.selectorView.transform = CGAffineTransformMakeTranslation(cell.selectorView.layer.anchorPoint.x - 50, 0);
            cell.descriptionLabel.transform = CGAffineTransformMakeTranslation(cell.descriptionLabel.layer.anchorPoint.x - 50, 0);
        }];
    }
}

- (void) dealloc {
    // Remove any possible duplicates
    NSMutableArray *networksToRemove = [[NSMutableArray alloc] init];
    for (WifiNetwork *network in self.knownNetworksArray) {
        for (WifiNetwork *stopNetwork in self.journeyStopNetworksArray) {
            if ([network.ssid isEqualToString:stopNetwork.ssid]) {
                [networksToRemove addObject:network];
            }
        }
    }
    [self.knownNetworksArray removeObjectsInArray:networksToRemove];
    
    // Save new configurations
    [Helpers saveObject:self.knownNetworksArray toDiskWithName:@"wifi"];
    [Helpers saveObject:self.journeyStopNetworksArray toDiskWithName:@"wifi-stop"];
}

@end
