//
//  SDJourneyDetection.h
//  socialdriver
//
//  Created by Ricardo on 18/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "JourneyDetection.h"

@interface SDJourneyDetection : JourneyDetection
+ (instancetype) sharedJourneyDetection;
@end
