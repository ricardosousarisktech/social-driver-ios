//
//  SDJourneyDetection.m
//  socialdriver
//
//  Created by Ricardo on 18/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "SDJourneyDetection.h"

@implementation SDJourneyDetection

+ (void)load
{
    [SDJourneyDetection sharedJourneyDetection];
}

+ (instancetype)sharedJourneyDetection {
    static SDJourneyDetection *sharedJourneyDetection = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedJourneyDetection = [[self alloc] init];
    });
    return sharedJourneyDetection;
}

- (id)init {
    self = [super init];
    return self;
}

- (void)dealloc {}

@end
