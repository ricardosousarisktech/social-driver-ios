//
//  StoryboardIdentifiers.h
//  socialdriver
//
//  Created by Ricardo on 24/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>

OBJC_EXTERN NSString * const IdentLogin;
OBJC_EXTERN NSString * const IdentVCDashboardNav;
OBJC_EXTERN NSString * const IdentVCDashboard;
OBJC_EXTERN NSString * const IdentVCDashboardMenu;
OBJC_EXTERN NSString * const IdentVCJourneys;
OBJC_EXTERN NSString * const IdentVCSettings;
OBJC_EXTERN NSString * const IdentVCMyInfo;

OBJC_EXTERN NSString * const IdentTVCMenu;
OBJC_EXTERN NSString * const IdentTVCJourney;
OBJC_EXTERN NSString * const IdentTVCMultiSelection;

OBJC_EXTERN NSString * const SeguePushDashboard;

OBJC_EXTERN NSString * const StoryboardLoginFlow;
OBJC_EXTERN NSString * const StoryboardDashboardFlow;