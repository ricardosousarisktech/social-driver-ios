//
//  StoryboardIdentifiers.m
//  socialdriver
//
//  Created by Ricardo on 24/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "StoryboardIdentifiers.h"

NSString * const IdentLogin = @"IdentLogin";
NSString * const IdentVCDashboardNav = @"IdentVCDashboardNav";
NSString * const IdentVCDashboard = @"IdentVCDashboard";
NSString * const IdentVCJourneys = @"IdentVCJourneys";
NSString * const IdentVCDashboardMenu = @"IdentVCDashboardMenu";
NSString * const IdentVCSettings = @"IdentVCSettings";
NSString * const IdentVCMyInfo = @"IdentVCMyInfo";

NSString * const IdentTVCMenu = @"IdentTVCMenu";
NSString * const IdentTVCJourney = @"IdentTVCJourney";
NSString * const IdentTVCMultiSelection = @"IdentTVCMultiSelection";

NSString * const StoryboardLoginFlow = @"Main";
NSString * const StoryboardDashboardFlow = @"Dashboard";