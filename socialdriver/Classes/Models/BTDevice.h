//
//  BluetoothDevice.h
//  socialdriver
//
//  Created by Ricardo on 13/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "BaseModel.h"

@interface BTDevice : BaseModel
@property (strong, nonatomic) NSString *UID;
@property (strong, nonatomic) NSString *deviceName;

- (instancetype)initWithUID:(NSString*)UID andDeviceName:(NSString*)deviceName;
@end
