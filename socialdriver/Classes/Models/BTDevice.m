//
//  BluetoothDevice.m
//  socialdriver
//
//  Created by Ricardo on 13/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "BTDevice.h"

@implementation BTDevice

- (instancetype)initWithUID:(NSString*)UID andDeviceName:(NSString*)deviceName {
    self.UID = UID;
    self.deviceName = deviceName;
    
    return self;
}
@end
