//
//  BaseModel.h
//  socialdriver

#import <Foundation/Foundation.h>

//models that conform to <Saveable> will use NSCoding to save state
@protocol Saveable <NSObject>

@end

@interface BaseModel : NSObject<NSCoding>

@property BOOL performingBatchUpdate;

- (void)describeInstance;

@end
