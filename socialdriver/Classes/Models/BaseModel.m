//
//  BaseModel.m
//  socialdriver

#import "BaseModel.h"
#import <objc/runtime.h>

@implementation BaseModel

- (id)init{
    self = [super init];
    {
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if(self)
    {
        NSArray *allProperties = [self allPropertyNames];
        for(NSString *property in allProperties)
        {
            id value = [decoder decodeObjectForKey:property];
            if(value)
            {
                [self setValue:value forKey:property];
            }
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    NSArray *allProperties = [self allPropertyNames];
    for(NSString *property in allProperties)
    {
        if([self valueForKey:property])
        {
            if([[self valueForKey:property] respondsToSelector:@selector(encodeWithCoder:)])
            {
                [encoder encodeObject:[self valueForKey:property] forKey:property];
            }
        }
    }
}

- (NSArray *)allPropertyNames
{
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableArray *rv = [NSMutableArray array];
    
    unsigned i;
    for (i = 0; i < count; i++)
    {
        objc_property_t property = properties[i];
        NSString *name = [NSString stringWithUTF8String:property_getName(property)];
        [rv addObject:name];
    }
    
    free(properties);
    
    return rv;
}

- (void)describeInstance
{
    NSArray *allProperties = [self allPropertyNames];
    NSLog(@"===== STARTING DESCRIBING %@ =======", NSStringFromClass(self.class));
    for(NSString *property in allProperties)
    {
        if([[self valueForKey:property] respondsToSelector:@selector(describeInstance)])
        {
            NSLog(@"%@: ==>", property);
            [[self valueForKey:property] performSelector:@selector(describeInstance)];
        }
        else
        {
            NSLog(@"%@: %@", property, [self valueForKey:property]);
        }
    }
    NSLog(@"===== FINISHED DESCRIBING %@ =======", NSStringFromClass(self.class));
}


@end
