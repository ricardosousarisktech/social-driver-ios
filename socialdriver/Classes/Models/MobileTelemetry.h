//
//  MobileTelemetry.h
//  socialdriver
//
//  Created by Ricardo on 20/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MobileTelemetry : NSObject
@property int *msgType;
@property NSString *gpsTime;
@property double *gpsLatitude;
@property double *gpsLongitude;
@property int *gpsOrientation;
@property double *gpsSpeed;
@property int *gpsFix;
@property int *accelDecel;

- (instancetype)initWithValues: (int)msgType time: (NSString*)gpsTime latitude: (double)latitude
                     longitude: (double)longitude orientation: (int)orientation speed: (double)speed fix: (int) fix accelDecel: (int) accelDecel;
@end
