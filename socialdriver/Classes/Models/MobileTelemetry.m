//
//  MobileTelemetry.m
//  socialdriver
//
//  Created by Ricardo on 20/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "MobileTelemetry.h"

@implementation MobileTelemetry
- (instancetype)initWithValues: (int)msgType time: (NSString*)gpsTime latitude: (double)latitude
                     longitude: (double)longitude orientation: (int)orientation speed: (double)speed fix: (int) fix accelDecel: (int) accelDecel {
    self.msgType = &(msgType);
    self.gpsTime = gpsTime;
    self.gpsLatitude = &(latitude);
    self.gpsLongitude = &(longitude);
    self.gpsOrientation = &(orientation);
    self.gpsSpeed = &(speed);
    self.gpsFix = &(fix);
    self.accelDecel = &(accelDecel);
    
    return self;
}
@end
