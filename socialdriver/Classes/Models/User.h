//
//  User.h
//  socialdriver
//
//  Created by Ricardo on 17/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "BaseModel.h"

@interface User : BaseModel
@property NSNumber *userId;
@property NSString *secret;
@property NSString *firstName;
@property NSString *lastName;
@property NSDate *dateOfBirth;
@property NSString *address;
@property NSString *postCode;
@property NSString *carType;
@property BOOL *notififyUser;

- (instancetype)createUserWithUserId: (NSString*)userId secret: (NSString*)secret;
@end
