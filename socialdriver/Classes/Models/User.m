//
//  User.m
//  socialdriver
//
//  Created by Ricardo on 17/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "User.h"

@implementation User

- (instancetype)createUserWithUserId: (NSNumber*)userId secret: (NSString*)secret {
    self.userId = userId;
    self.secret = secret;
    
    return self;
}

@end
