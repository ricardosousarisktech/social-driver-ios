//
//  WifiNetwork.h
//  socialdriver
//
//  Created by Ricardo on 10/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface WifiNetwork : BaseModel
@property (strong, nonatomic) NSString *ssid;

- (instancetype)initWithSSID:(NSString*)ssid;
@end
