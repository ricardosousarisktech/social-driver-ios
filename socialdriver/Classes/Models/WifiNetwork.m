//
//  WifiNetwork.m
//  socialdriver
//
//  Created by Ricardo on 10/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "WifiNetwork.h"

@implementation WifiNetwork

- (instancetype)initWithSSID:(NSString*)ssid {
    self.ssid = ssid;
    return self;
}

@end
