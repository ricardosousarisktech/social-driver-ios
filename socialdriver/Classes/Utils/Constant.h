//
//  Constants.h
//  socialdriver
//
//  Created by Ricardo on 27/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>

OBJC_EXTERN NSString * const MenuItemHomeTitle;
OBJC_EXTERN NSString * const MenuItemJourneysTitle;
OBJC_EXTERN NSString * const MenuItemSettingsTitle;
OBJC_EXTERN NSString * const MenuItemMyInformationTitle;
OBJC_EXTERN NSString * const MenuItemLogoutTitle;

OBJC_EXTERN NSString * const MenuItemHomeImage;
OBJC_EXTERN NSString * const MenuItemJourneysImage;
OBJC_EXTERN NSString * const MenuItemSettingsImage;
OBJC_EXTERN NSString * const MenuItemMyInformationImage;
OBJC_EXTERN NSString * const MenuItemLogoutImage;

OBJC_EXTERN NSString * const WheelAgeIcon;
OBJC_EXTERN NSString * const WheelCarIcon;
OBJC_EXTERN NSString * const WheelGenderIcon;
OBJC_EXTERN NSString * const WheelLocationIcon;

OBJC_EXTERN NSString * const DevAPIURL;
OBJC_EXTERN NSString * const LoginURL;
OBJC_EXTERN NSString * const CreateUserURL;
OBJC_EXTERN NSString * const ProcessJourney;
OBJC_EXTERN NSString * const AppID;

@interface Contants : NSObject
+ (NSString*)processJourneyURL: (NSString*)userID signature: (NSString*)signature;
@end
