//
//  Constants.m
//  socialdriver
//
//  Created by Ricardo on 27/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "Constant.h"

NSString * const MenuItemHomeTitle = @"Home";
NSString * const MenuItemJourneysTitle = @"Journeys";
NSString * const MenuItemSettingsTitle = @"Settings";
NSString * const MenuItemMyInformationTitle = @"My Information";
NSString * const MenuItemLogoutTitle = @"Logout";

NSString * const MenuItemHomeImage = @"home-icon";
NSString * const MenuItemJourneysImage = @"journeys-icon";
NSString * const MenuItemSettingsImage = @"settings-icon";
NSString * const MenuItemMyInformationImage = @"my-information";
NSString * const MenuItemLogoutImage = @"logout-icon";

NSString * const WheelAgeIcon = @"age-icon";
NSString * const WheelCarIcon = @"car-icon";
NSString * const WheelGenderIcon = @"gender-icon";
NSString * const WheelLocationIcon = @"journeys-icon";

NSString * const DevAPIURL = @"http://rtl-dev-mobileapi.elasticbeanstalk.com";
NSString * const LoginURL = @"/api/socialdriver/login";
NSString * const CreateUserURL = @"/api/socialdriver/createuser";
NSString * const ProcessJourney = @"/api/socialdriver/processjourney";
NSString * const AppID = @"2";

@implementation Contants

+ (NSString*)processJourneyURL: (NSString*)userID signature: (NSString*)signature {
    NSString *processJourneyWithParams = [NSString stringWithFormat:@"%@?userid=%@&signature=%@", ProcessJourney, userID, signature];
    return processJourneyWithParams;
}

@end