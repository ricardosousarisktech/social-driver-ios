//
//  Helpers.h
//  socialdriver
//
//  Created by Ricardo on 27/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;
@import SystemConfiguration.CaptiveNetwork;

@interface Helpers : NSObject

+ (UIStoryboard *)getStoryboardForIdentifier:(NSString *)storyboardIdentifier;
+ (UIViewController *)getViewControllerForIdentifier:(NSString *)identifier fromStoryboardWithIdentifier:(NSString *)storyboardIdentifier;
+ (UIViewController *)getViewControllerForIdentifier:(NSString *)identifier fromStoryboard:(UIStoryboard *)storyboard;
+ (UIImage*)resizeThisImage:(UIImage*)image scaledToSize:(CGSize)newSize;
+ (void)lookForNewWifiNetworks;
+ (BOOL)prepareAudioSession;
+ (void)scanForBluetoothDevices;
+ (BOOL)isConnectedToBluetoothDevice;

+ (void)saveObject:(id<NSCoding>)object toDiskWithName:(NSString *)name;

+ (id<NSCoding>)loadObjectWithName:(NSString *)name;

+ (void)clearObjectWithName:(NSString *)name;

//only use these for objects that can be recreated at any time
+ (void)cacheObject:(id<NSCoding>)object toDiskWithName:(NSString *)name;

+ (id<NSCoding>)loadCachedObjectWithName:(NSString *)name;

+ (NSString *)hmacsha1:(NSString *)data secret:(NSString *)key;

+ (NSString*)generateSecretSignature:(NSDictionary*)attributesDictionary;

+ (void)displayAnOKAlertDialogWithTitle: (NSString*)title andBody: (NSString*)text;

+ (NSString*)currentTimezone;
+ (NSString*)deviceLanguage;
+ (void)saveUserCredentials: (NSString*)userid secret: (NSString*)secret;
@end
