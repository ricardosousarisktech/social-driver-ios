//
//  Helpers.m
//  socialdriver
//
//  Created by Ricardo on 27/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "Helpers.h"
#import "StoryboardIdentifiers.h"
#import "WifiNetwork.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#include <CommonCrypto/CommonHMAC.h>
#import "BTDevice.h"
#import "SDJourneyDetection.h"

@implementation Helpers

+ (UIStoryboard *)getStoryboardForIdentifier:(NSString *)storyboardIdentifier;
{
    return [UIStoryboard storyboardWithName:storyboardIdentifier bundle:[NSBundle mainBundle]];
}

+ (UIViewController *)getViewControllerForIdentifier:(NSString *)identifier fromStoryboardWithIdentifier:(NSString *)storyboardIdentifier
{
    UIStoryboard *storyboard = [Helpers getStoryboardForIdentifier:storyboardIdentifier];
    return [Helpers getViewControllerForIdentifier:identifier fromStoryboard:storyboard];
}

+ (UIViewController *)getViewControllerForIdentifier:(NSString *)identifier fromStoryboard:(UIStoryboard *)storyboard
{
    return [storyboard instantiateViewControllerWithIdentifier:identifier];
}

+ (UIImage*)resizeThisImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)dummy
{
    if(![NSThread isMainThread])
    {
        [self performSelectorInBackground:_cmd withObject:nil]; // <-- One method of running on BG Thread - only allows selector based calls.
        
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            //Anything here is background
        
            dispatch_async(dispatch_get_main_queue(), ^{
                //anything here is back on the main thread
            });
    });
}

+ (void)lookForNewWifiNetworks {
    NSMutableArray *wifiNetworks = (NSMutableArray *)[Helpers loadObjectWithName:@"wifi"];
    [wifiNetworks addObjectsFromArray:(NSArray*)[Helpers loadCachedObjectWithName:@"wifi-stop"]];
    NSDictionary *currentNetwork = [Helpers fetchSSIDInfo];
    if (wifiNetworks.count > 0) {
        if (currentNetwork) {
            BOOL exists = NO;
            WifiNetwork *network = [[WifiNetwork alloc] initWithSSID:currentNetwork[@"SSID"]];
            for (WifiNetwork *tempNetwork in wifiNetworks) {
                if ([tempNetwork.ssid isEqualToString:network.ssid]) {
                    exists = YES;
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"autojourney"]) {
                        [[SDJourneyDetection sharedJourneyDetection] stopJourney];
                        [[SDJourneyDetection sharedJourneyDetection] stopMonitoring];
                    }
                    break;
                }
            }
            if (!exists) {
                [wifiNetworks addObject:network];
                [Helpers saveObject:wifiNetworks toDiskWithName:@"wifi"];
            }
        }
    } else if (currentNetwork) {
        WifiNetwork *network = [[WifiNetwork alloc] initWithSSID:currentNetwork[@"SSID"]];
        wifiNetworks = [NSMutableArray arrayWithObjects:network, nil];
        [Helpers saveObject:wifiNetworks toDiskWithName:@"wifi"];
    }
}

+ (NSDictionary *)fetchSSIDInfo {
    NSArray *interfaceNames = CFBridgingRelease(CNCopySupportedInterfaces());
    
    NSDictionary *SSIDInfo;
    for (NSString *interfaceName in interfaceNames) {
        SSIDInfo = CFBridgingRelease(CNCopyCurrentNetworkInfo((__bridge CFStringRef)interfaceName));
        
        BOOL isNotEmpty = (SSIDInfo.count > 0);
        if (isNotEmpty) {
            break;
        }
    }
    return SSIDInfo;
}

#pragma mark - Bluetooth check methods
+ (BOOL)prepareAudioSession {
    // deactivate session
    BOOL success = [[AVAudioSession sharedInstance] setActive:NO error: nil];
    if (!success) {
        NSLog(@"deactivationError");
    }
    
    // set audio session category AVAudioSessionCategoryPlayAndRecord options AVAudioSessionCategoryOptionAllowBluetooth
    success = [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:nil];
    if (!success) {
        NSLog(@"setCategoryError");
    }
    
    // activate audio session
    success = [[AVAudioSession sharedInstance] setActive:YES error: nil];
    if (!success) {
        NSLog(@"activationError");
    }
    
    return success;
}

+ (void)scanForBluetoothDevices {
    NSArray *availableInputs = [[AVAudioSession sharedInstance] availableInputs];
    NSMutableArray *savedInputs = (NSMutableArray*)[Helpers loadObjectWithName:@"bluetooth"];
    [savedInputs addObjectsFromArray:(NSArray*)[Helpers loadObjectWithName:@"bluetooth-start"]];
    NSInteger count = availableInputs.count;
    if (savedInputs) {
        for (int k = 0; k < count; k++) {
            AVAudioSessionPortDescription *portDesc = [availableInputs objectAtIndex:k];
            if ([portDesc.portType hasPrefix:@"Bluetooth"]) {
                BOOL exists = NO;
                for (BTDevice *bt in savedInputs) {
                    if ([bt.UID isEqualToString:portDesc.UID]) {
                        exists = YES;
                        break;
                    }
                }
                if (!exists) {
                    BTDevice *newDevice = [[BTDevice alloc] initWithUID:portDesc.UID andDeviceName:portDesc.portName];
                    [savedInputs addObject: newDevice];
                    [Helpers saveObject:savedInputs toDiskWithName:@"bluetooth"];
                }
            }
        }
    } else if (availableInputs) {
        for (AVAudioSessionPortDescription *portDesc in availableInputs) {
            if ([portDesc.portType hasPrefix:@"Bluetooth"]) {
                BTDevice *newDevice = [[BTDevice alloc] initWithUID:portDesc.UID andDeviceName:portDesc.portName];
                if (!savedInputs) {
                    savedInputs = [NSMutableArray arrayWithObjects:newDevice, nil];
                } else {
                    [savedInputs addObject:newDevice];
                }
            }
        }
        [Helpers saveObject:savedInputs toDiskWithName:@"bluetooth"];
    }
}

+ (BOOL)isConnectedToBluetoothDevice {
    NSArray *availableInputs = [[AVAudioSession sharedInstance] availableInputs];
    for (int i = 0; i < availableInputs.count; i++) {
        AVAudioSessionPortDescription *portDesc = [availableInputs objectAtIndex:i];
        if ([portDesc.portType hasPrefix:@"Bluetooth"]) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - Airplay Devices
- (NSString*)activeAirplayOutputRouteName {
    AVAudioSession* audioSession = [AVAudioSession sharedInstance];
    AVAudioSessionRouteDescription* currentRoute = audioSession.currentRoute;
    for (AVAudioSessionPortDescription* outputPort in currentRoute.outputs){
        NSArray *channels = outputPort.channels;
        NSLog(@"This is channel one %@", channels[0]);
        if ([outputPort.portType isEqualToString:AVAudioSessionPortAirPlay] ||
            [outputPort.portType isEqualToString:AVAudioSessionPortCarAudio] ||
            [outputPort.portType isEqualToString:AVAudioSessionPortBluetoothA2DP] ||
            [outputPort.portType isEqualToString:AVAudioSessionPortBluetoothHFP] ||
            [outputPort.portType isEqualToString:AVAudioSessionPortBluetoothLE])
            return outputPort.portName;
    }
    
    return nil;
}

+ (NSString *)getDocsDir {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"data"];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory isDirectory:nil])
    {
        
        NSError *error;
        [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    return documentsDirectory;
    
}
+ (NSString *)getCacheDir {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDirectory = [paths objectAtIndex:0];
    cacheDirectory = [cacheDirectory stringByAppendingPathComponent:@"data"];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:cacheDirectory isDirectory:nil])
    {
        NSError *error;
        [[NSFileManager defaultManager] createDirectoryAtPath:cacheDirectory withIntermediateDirectories:YES attributes:nil error:&error];
    }
    return cacheDirectory;
    
}

+ (void)saveObject:(id<NSCoding>)object toDiskWithName:(NSString *)name
{
    NSString *dataPath = [[Helpers getDocsDir]stringByAppendingPathComponent:name];
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:object forKey:name];
    [archiver finishEncoding];
    [data writeToFile:dataPath atomically:YES];
}

+ (id<NSCoding>)loadObjectWithName:(NSString *)name
{
    NSString *dataPath = [[Helpers getDocsDir] stringByAppendingPathComponent:name];
    NSData *codedData = [[NSData alloc] initWithContentsOfFile:dataPath];
    if (codedData == nil) return nil;
    
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:codedData];
    id object = [unarchiver decodeObjectForKey:name];
    return object;
}

+ (void)clearObjectWithName:(NSString *)name {
    NSString *dataPath = [[Helpers getDocsDir]stringByAppendingPathComponent:name];
    // Empty mutable array:
    NSMutableData *data = [[NSMutableData alloc] init];
    [data writeToFile:dataPath atomically:YES];
}

+ (void)cacheObject:(id<NSCoding>)object toDiskWithName:(NSString *)name
{
    NSString *dataPath = [[Helpers getCacheDir]stringByAppendingPathComponent:name];
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:object forKey:name];
    [archiver finishEncoding];
    [data writeToFile:dataPath atomically:YES];
}

+ (id<NSCoding>)loadCachedObjectWithName:(NSString *)name
{
    NSString *dataPath = [[Helpers getCacheDir] stringByAppendingPathComponent:name];
    NSData *codedData = [[NSData alloc] initWithContentsOfFile:dataPath];
    if (codedData == nil) return nil;
    
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:codedData];
    id object = [unarchiver decodeObjectForKey:name];
    return object;
}

+ (NSString *)hmacsha1:(NSString *)data secret:(NSString *)key {
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSASCIIStringEncoding];
    
    unsigned char cHMAC[CC_SHA1_DIGEST_LENGTH];
    
    CCHmac(kCCHmacAlgSHA1, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *HMACData = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    
    
    const unsigned char *buffer = (const unsigned char *)[HMACData bytes];
    NSMutableString *HMAC = [NSMutableString stringWithCapacity:HMACData.length * 2];
    for (int i = 0; i < HMACData.length; ++i){
        [HMAC appendFormat:@"%02x", buffer[i]];
    }
    
    return HMAC;
}

+ (NSString*)generateSecretSignature:(NSDictionary*)attributesDictionary {
    // TODO Implement this method please!
    return @"";
}

+ (void)displayAnOKAlertDialogWithTitle: (NSString*)title andBody: (NSString*)text {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"turnOffIndicator" object:self];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:text
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
}

+ (NSString*)currentTimezone {
    return [NSTimeZone systemTimeZone].name;
}

+ (NSString*)deviceLanguage {
    return [[NSLocale preferredLanguages] objectAtIndex:0];
}

+ (void)saveUserCredentials: (NSString*)userid secret: (NSString*)secret {
    [[NSUserDefaults standardUserDefaults] setObject:userid forKey:@"activeuser"];
    [[NSUserDefaults standardUserDefaults] setObject:secret forKey:@"secret"];
}
@end
