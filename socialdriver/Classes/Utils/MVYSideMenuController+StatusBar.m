//
//  MVYSideMenuController+StatusBar.m
//  socialdriver
//
//  Created by Ricardo on 24/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "MVYSideMenuController+StatusBar.h"

@implementation MVYSideMenuController (StatusBar)

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
