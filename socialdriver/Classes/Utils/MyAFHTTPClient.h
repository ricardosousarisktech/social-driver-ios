//
//  MyRestKit.h
//  socialdriver
//
//  Created by Ricardo on 20/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "AFHTTPClient.h"

@interface MyAFHTTPClient : AFHTTPClient
+ (instancetype) sharedInstance;
@end
