//
//  MyRestKit.m
//  socialdriver
//
//  Created by Ricardo on 20/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "MyAFHTTPClient.h"
#import "Constant.h"


@implementation MyAFHTTPClient
+ (void)load {
    [MyAFHTTPClient sharedInstance];
}

+ (instancetype)sharedInstance {
    static MyAFHTTPClient *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[super alloc] initWithBaseURL:[NSURL URLWithString:DevAPIURL]];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    return self;
}

- (void)dealloc {}
@end
