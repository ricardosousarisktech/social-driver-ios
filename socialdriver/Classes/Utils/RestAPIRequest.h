//
//  APIRequest.h
//  socialdriver
//
//  Created by Ricardo on 17/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RKObjectManager.h"
#import "MobileTelemetry.h"
#import "LoginResponse.h"

@interface RestAPIRequest : NSObject
+ (RKObjectManager*) createUserUsingFacebook;
+ (RKObjectManager*) createSocialDriverUser;
+ (RKObjectManager*) loginUser;
+ (RKObjectManager*) loginUserWithFacebook;
+ (RKObjectManager*) sendJourneyToProcess: (NSString*)signature;
@end
