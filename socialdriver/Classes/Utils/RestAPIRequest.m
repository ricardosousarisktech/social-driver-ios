//
//  APIRequest.m
//  socialdriver
//
//  Created by Ricardo on 17/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "RestAPIRequest.h"
#import "AFHTTPClient.h"
#import "Constant.h"
#import "RKRequestDescriptor.h"
#import "RKResponseDescriptor.h"
#import "CreateUserRequest.h"
#import "RKErrorMessage.h"
#import "Helpers.h"
#import "LoginUserRequest.h"
#import "MyAFHTTPClient.h"
#import "ProcessJourneyRequest.h"
#import "LoginResponse.h"
#import "RKRelationshipMapping.h"

@implementation RestAPIRequest

+ (RKObjectManager*) createUserUsingFacebook {
    // Init RestKit object manager
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:[MyAFHTTPClient sharedInstance]];

    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromArray:@[@"oAuthid", @"appId", @"timezone", @"culture"]];
    
    RKRequestDescriptor *requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:requestMapping
                                                                                   objectClass:[CreateUserRequest class]
                                                                                   rootKeyPath:nil
                                                                                        method:RKRequestMethodPOST];
    [objectManager addRequestDescriptor:requestDescriptor];
    [objectManager addResponseDescriptor:[self getCreateResponseDescriptor]];
    
    return objectManager;
}

+ (RKObjectManager*) createSocialDriverUser {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    // Init RestKit object manager
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:[MyAFHTTPClient sharedInstance]];

    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromArray:@[@"username", @"password", @"appId", @"timezone", @"culture"]];

    // Request Descriptor
    RKRequestDescriptor *requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:requestMapping
                                                                                   objectClass:[CreateUserRequest class]
                                                                                   rootKeyPath:nil
                                                                                        method:RKRequestMethodPOST];
    [objectManager addRequestDescriptor:requestDescriptor];
    [objectManager addResponseDescriptor:[self getCreateResponseDescriptor]];
    
    return objectManager;
}

+ (RKObjectManager*) loginUser {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:[MyAFHTTPClient sharedInstance]];
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromArray:@[@"username", @"password", @"appID"]];
    
    RKRequestDescriptor *requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:requestMapping
                                                                                   objectClass:[LoginUserRequest class]
                                                                                   rootKeyPath:nil method:RKRequestMethodPOST];
    [objectManager addRequestDescriptor:requestDescriptor];
    [objectManager addResponseDescriptor:[self getLoginResponseDescriptor]];
    
    return objectManager;
}

+ (RKObjectManager*) loginUserWithFacebook {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:[MyAFHTTPClient sharedInstance]];
    
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromArray:@[@"oAuthId", @"appID"]];
    
    RKRequestDescriptor *requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:requestMapping
                                                                                   objectClass:[LoginUserRequest class]
                                                                                   rootKeyPath:nil
                                                                                        method:RKRequestMethodPOST];
    [objectManager addRequestDescriptor:requestDescriptor];
    
    [objectManager addResponseDescriptor:[self getFBLoginResponseDescriptor]];
        
    return objectManager;
}

+ (RKObjectManager*) sendJourneyToProcess: (NSString*)signature {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:[MyAFHTTPClient sharedInstance]];
    RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[APIResponse class]];
    [responseMapping addAttributeMappingsFromDictionary:@{
                                                          @"Success": @"success",
                                                          @"ErrorCode": @"errorCode",
                                                          @"ErrorDescription": @"errorDescription"
                                                          }];
    
    RKRequestDescriptor *requestDescriptor = [self getProcessJourneyRequestDescriptor];
    
    [objectManager addRequestDescriptor:requestDescriptor];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:ProcessJourney
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:responseDescriptor];
    
    return objectManager;
}

+ (RKResponseDescriptor*) getFBLoginResponseDescriptor {
    RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[LoginResponse class]];
    [responseMapping addAttributeMappingFromKeyOfRepresentationToAttribute:@"UserSocialDriver"];
    [responseMapping addAttributeMappingsFromDictionary:@{
                                                          @"(UserSocialDriver).UserId": @"userId",
                                                          @"(UserSocialDriver).Secret": @"secret",
                                                          @"Success": @"success",
                                                          @"ErrorCode": @"errorCode",
                                                          @"ErrorDescription": @"errorDescription"
                                                          }];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:LoginURL keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    return responseDescriptor;
}

+ (RKResponseDescriptor*) getLoginResponseDescriptor {
    RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[LoginResponse class]];
    [responseMapping addAttributeMappingsFromDictionary:@{
                                                          @"UserId": @"userId",
                                                          @"Secret": @"secret",
                                                          @"Success": @"success",
                                                          @"ErrorCode": @"errorCode",
                                                          @"ErrorDescription": @"errorDescription"
                                                          }];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:LoginURL keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    return responseDescriptor;
}

+ (RKResponseDescriptor*) getCreateResponseDescriptor {
    RKObjectMapping *responseMapping = [RKObjectMapping mappingForClass:[LoginResponse class]];
    [responseMapping addAttributeMappingsFromDictionary:@{
                                                          @"UserId": @"userId",
                                                          @"SecretKey": @"secret",
                                                          @"Success": @"success",
                                                          @"ErrorCode": @"errorCode",
                                                          @"ErrorDescription": @"errorDescripton"
                                                          }];
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:responseMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:CreateUserURL keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    return responseDescriptor;
}

+ (RKRequestDescriptor*) getProcessJourneyRequestDescriptor {
    // Create mapping for the Mobile telemetry object
    RKObjectMapping *telemetryMapping = [RKObjectMapping requestMapping];
    [telemetryMapping addAttributeMappingsFromDictionary:@{
                                                           @"msgType": @"MsgType",
                                                           @"gpsTime": @"GpsTime",
                                                           @"gpsLatitude": @"GpsLatitude",
                                                           @"gpsLongitude": @"GpsLongitude",
                                                           @"gpsOrientation": @"GpsOrientation",
                                                           @"gpsSpeed": @"GpsSpeed",
                                                           @"gpsFix": @"GpsFix",
                                                           @"accelDecel": @"AccelDecel"
                                                           }];
    RKObjectMapping *requestMapping = [RKObjectMapping requestMapping];
    [requestMapping addAttributeMappingsFromDictionary:@{
                                                         @"userid" : @"UserId",
                                                         @"minsToTrim": @"MinsToTrim"
                                                         }];
    [requestMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"mobileTelemetry"
                                                                                   toKeyPath:@"MobileTelemetry"
                                                                                 withMapping:telemetryMapping]];
    
    RKRequestDescriptor *requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:requestMapping
                                                                                 objectClass:[ProcessJourneyRequest class]
                                                                                 rootKeyPath:nil method:RKRequestMethodPOST];
    
    return requestDescriptor;
}
@end
