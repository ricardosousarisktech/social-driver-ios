//
//  RotaryProtocol.h
//  socialdriver
//
//  Created by Ricardo on 27/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RotaryProtocol <NSObject>

- (void) wheelDidChangeValue:(NSString *)newValue;

@end
