//
//  UIColor+SocialDriver.h
//  socialdriver
//
//  Created by Ricardo on 23/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (SocialDriverColors)
+ (UIColor*) textFieldsColor;
+ (UIColor*) segmentOriginalColor;
+ (UIColor*) segmentHighlighedColor;
+ (UIColor*) titleBackgroundBlueColor;
+ (UIColor*) badRedColor;
+ (UIColor*) settingsGreyFontColor;
+ (UIColor*) greyButtonColor;
@end