//
//  UIColor+SocialDriver.m
//  socialdriver
//
//  Created by Ricardo on 23/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIColor+SocialDriver.h"
#import <UIKit/UIKit.h>

@implementation UIColor (SocialDriverColors)

+ (UIColor*) textFieldsColor {
    return [UIColor colorWithWhite:1.0 alpha:1.0];
}

+ (UIColor*) segmentOriginalColor {
    return [UIColor colorWithRed:27.0/255 green:167.0/255 blue:209.0/255 alpha:1.0];
}

+ (UIColor*) segmentHighlighedColor {
    return [UIColor colorWithRed:67.0/255 green:139.0/255 blue:181.0/255 alpha:1];
}

+ (UIColor*) titleBackgroundBlueColor {
    return [UIColor colorWithRed:80.0/255 green:165.0/255 blue:215.0/255 alpha:1];
}

+ (UIColor*) badRedColor {
    return [UIColor colorWithRed:249.0/255 green:18.0/255 blue:47.0/255 alpha:1];
}

+ (UIColor*) settingsGreyFontColor {
    return [UIColor colorWithRed:190.0/255 green:190.0/255 blue:190.0/255 alpha:1.0];
}

+ (UIColor*) greyButtonColor {
    return [UIColor colorWithRed:155.0/255 green:155.0/255 blue:155.0/255 alpha:1.0];
}

@end