//
//  Clove.h
//  socialdriver
//
//  Created by Ricardo on 27/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Clove : NSObject
@property float minValue;
@property float maxValue;
@property float midValue;
@property int value;
@end
