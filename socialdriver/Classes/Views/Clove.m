//
//  Clove.m
//  socialdriver
//
//  Created by Ricardo on 27/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "Clove.h"

@implementation Clove
@synthesize minValue, maxValue, midValue, value;

- (NSString *) description {
    // Log clove's position
    return [NSString stringWithFormat:@"%i | %f, %f, %f", self.value, self.minValue, self.midValue, self.maxValue];
    
}
@end
