//
//  JourneyTableViewCell.h
//  socialdriver
//
//  Created by Ricardo on 04/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JourneyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *overallScoreView;
@property (weak, nonatomic) IBOutlet UIView *breakingScoreView;
@property (weak, nonatomic) IBOutlet UIView *accelScoreView;
@property (weak, nonatomic) IBOutlet UIView *speedScoreView;
@property (weak, nonatomic) IBOutlet UILabel *journeyNameDummyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *playIcon;
@property (weak, nonatomic) IBOutlet UIImageView *stopIcon;
@property (weak, nonatomic) IBOutlet UIImageView *travelTimeIcon;
@property (weak, nonatomic) IBOutlet UIImageView *distanceIcon;

@end
