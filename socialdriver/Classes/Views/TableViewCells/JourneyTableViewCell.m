//
//  JourneyTableViewCell.m
//  socialdriver
//
//  Created by Ricardo on 04/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "JourneyTableViewCell.h"
#import "UIImage+Tint.h"
#import "UIColor+SocialDriver.h"

@implementation JourneyTableViewCell

- (void)awakeFromNib {
    self.overallScoreView.layer.cornerRadius = self.overallScoreView.bounds.size.width / 2;
    self.breakingScoreView.layer.cornerRadius = self.breakingScoreView.bounds.size.width / 2;
    self.breakingScoreView.layer.borderColor = [UIColor redColor].CGColor;
    self.breakingScoreView.layer.borderWidth = 1.0;
    self.accelScoreView.layer.cornerRadius = self.accelScoreView.bounds.size.width / 2;
    self.accelScoreView.layer.borderColor = [UIColor yellowColor].CGColor;
    self.accelScoreView.layer.borderWidth = 1.0;
    self.speedScoreView.layer.cornerRadius = self.speedScoreView.bounds.size.width / 2;
    self.speedScoreView.layer.borderColor = [UIColor greenColor].CGColor;
    self.speedScoreView.layer.borderWidth = 1.0;
    self.playIcon.image = [self.playIcon.image imageTintedWithColor:[UIColor blackColor]];
    self.stopIcon.image = [self.stopIcon.image imageTintedWithColor:[UIColor blackColor]];
    self.distanceIcon.image = [self.distanceIcon.image imageTintedWithColor:[UIColor blackColor]];
    self.travelTimeIcon.image = [self.travelTimeIcon.image imageTintedWithColor:[UIColor blackColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
