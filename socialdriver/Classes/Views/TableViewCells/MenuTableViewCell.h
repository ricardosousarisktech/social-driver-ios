//
//  MenuTableViewCell.h
//  socialdriver
//
//  Created by Ricardo on 27/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *menuLabel;
@end
