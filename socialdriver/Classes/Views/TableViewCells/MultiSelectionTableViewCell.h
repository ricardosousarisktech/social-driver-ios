//
//  MultiSelectionTableViewCell.h
//  socialdriver
//
//  Created by Ricardo on 07/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultiSelectionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *selectorView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@end
