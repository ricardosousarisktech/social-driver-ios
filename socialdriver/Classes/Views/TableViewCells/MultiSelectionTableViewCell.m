//
//  MultiSelectionTableViewCell.m
//  socialdriver
//
//  Created by Ricardo on 07/08/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import "MultiSelectionTableViewCell.h"
#import "UIColor+SocialDriver.h"

@implementation MultiSelectionTableViewCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.selectorView.layer.cornerRadius = self.selectorView.bounds.size.width / 2;
    self.selectorView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.selectorView.layer.borderWidth = 1.0;
    // Hide selector view
    self.selectorView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    self.descriptionLabel.layer.anchorPoint = CGPointMake(0.5, 0.5);
    self.selectorView.transform = CGAffineTransformMakeTranslation(self.selectorView.frame.origin.x - 120, 0);
    self.descriptionLabel.transform = CGAffineTransformMakeTranslation(self.descriptionLabel.frame.origin.x - 120, 0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
