//
//  TheWheel.h
//  socialdriver
//
//  Created by Ricardo on 27/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RotaryProtocol.h"

@interface TheWheel : UIControl

@property (weak) id <RotaryProtocol> delegate;
@property (nonatomic, strong) UIView *container;
@property CGAffineTransform startTransform;
@property (nonatomic, strong) NSMutableArray *cloves;
@property int currentValue;


- (id) initWithFrame:(CGRect)frame andDelegate:(id)del;
@end
