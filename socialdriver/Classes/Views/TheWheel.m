//
//  TheWheel.m
//  socialdriver
//
//  Created by Ricardo on 27/07/2015.
//  Copyright (c) 2015 Risk. All rights reserved.
//

@import CoreGraphics;
#import <QuartzCore/QuartzCore.h>
#import "TheWheel.h"
#import "Constant.h"
#import "Clove.h"
#import <MGImageUtilities/UIImage+ProportionalFill.h>
#import <MGImageUtilities/UIImage+Tint.h>
#import "UIColor+SocialDriver.h"

typedef enum {
    Location,
    Age,
    Car,
    Gender,
    IconsCount
}SegmentIcons;

@interface TheWheel ()
- (void) drawWheel;
- (float) calculateDistanceFromCenter:(CGPoint)point;
- (void) buildClovesEven;
- (UIImageView *) getCloveByValue:(int)value;
- (NSString *) getCloveName:(int)position;
@end

static float deltaAngle;
static float topCloveAngle;
static float minAlphavalue = 1.0;
static float maxAlphavalue = 1.0;
BOOL continuedGesture = NO;

@implementation TheWheel

@synthesize container, delegate, startTransform, cloves, currentValue;

- (id) initWithFrame:(CGRect)frame andDelegate:(id)del {

    self = [super initWithFrame:frame];
    if (self) {
        float dx = frame.size.width / 2;
        float dy = 0;
        topCloveAngle = atan2(dy, dx);
        
        self.currentValue = 1;
        self.delegate = del;
        [self drawWheel];
    }
    return self;
}

- (void) drawWheel {
    container = [[UIView alloc] initWithFrame:self.bounds];
    CGFloat angleSize = (2.0f * M_PI) / (CGFloat)IconsCount;
    
    for (int i = 0; i < IconsCount; i++) {
        UIImage *segmentImage = [UIImage imageNamed:@"segment"];
        CGFloat smallestEdge = MIN(self.frame.size.width, self.frame.size.height); //300
        CGFloat imageWidth = smallestEdge / 2;
        CGFloat ratio = imageWidth / segmentImage.size.width;
        CGSize imageSize = CGSizeApplyAffineTransform(segmentImage.size, CGAffineTransformMakeScale(ratio, ratio));
        
        segmentImage = [segmentImage imageScaledToFitSize:imageSize];
        
        UIImageView *segment = [[UIImageView alloc] initWithImage:segmentImage];
        segment.layer.anchorPoint = CGPointMake(1.0f, 0.5f);
        switch ((SegmentIcons)i) {
            case Age:
                segment.layer.position = CGPointMake(roundf(container.bounds.size.width/2.0), roundf(container.bounds.size.height/2.0) - 2);
                segment.image = [segment.image imageTintedWithColor:[UIColor segmentHighlighedColor]];
                break;
            case Car:
                segment.layer.position = CGPointMake(roundf(container.bounds.size.width/2.0) + 2, roundf(container.bounds.size.height/2.0));
                break;
            case Gender:
                segment.layer.position = CGPointMake(roundf(container.bounds.size.width/2.0), roundf(container.bounds.size.height/2.0) + 2);
                break;
            case Location:
                segment.layer.position = CGPointMake(roundf(container.bounds.size.width/2.0) - 2, roundf(container.bounds.size.height/2.0));
                break;
                
            default:
                break;
        }
        segment.alpha = minAlphavalue; 
        segment.tag = i;
        segment.contentMode = UIViewContentModeCenter;

        
        segment.transform = CGAffineTransformMakeRotation(angleSize*i);
        
        if (i == 0) {
            segment.alpha = maxAlphavalue;
        }
        
        // CHANGE THE ICONS POSITION AND SIZE
        UIImageView *cloveImage = [[UIImageView alloc] initWithFrame:CGRectMake(segment.image.size.height / 9, segment.image.size.width / 1.7, segmentImage.size.width / 4, segmentImage.size.width / 4)];
        switch ((SegmentIcons)i) {
            case Age:
                cloveImage.image = [UIImage imageNamed:WheelAgeIcon];
                break;
            case Car:
                cloveImage.image = [UIImage imageNamed:WheelCarIcon];
                break;
            case Gender:
                cloveImage.image = [UIImage imageNamed:WheelGenderIcon];
                break;
            case Location:
                cloveImage.image = [UIImage imageNamed:WheelLocationIcon];
                break;
            default:
                break;
        }
        cloveImage.image = [cloveImage.image imageTintedWithColor:[UIColor whiteColor]];
        cloveImage.transform = CGAffineTransformMakeRotation(-M_PI_2);
        
        cloveImage.tag = i;
        [segment addSubview:cloveImage];
        [container addSubview:segment];
    }
    
    container.userInteractionEnabled = NO;
    container.layer.position = CGPointMake(container.layer.position.x, container.layer.position.y);
    [self addSubview:container];
    
    cloves = [NSMutableArray arrayWithCapacity:IconsCount];
    
    // Centre button:
//    UIImageView *mask = [[UIImageView alloc] initWithFrame:CGRectMake(container.bounds.size.width / 2.0, container.bounds.size.height / 2.0, 58, 58)];
//    CGRect circleRect = CGRectMake(0, 0, 58, 58);
//    circleRect = CGRectInset(circleRect, 5, 5);
//    mask.frame = circleRect;
//    [self addSubview:mask];
    
    [self buildClovesEven];
    
    [self.delegate wheelDidChangeValue:[self getCloveName:currentValue]];
}

- (UIImageView *) getCloveByValue:(int)value {
    UIImageView *result;
    NSArray *views = [container subviews];
    for (UIImageView *img in views) {
        if (img.tag == value)
            result = img;
    }
    return result;
}

- (UIImageView *) getIconImageByValue:(int)value {
    UIImageView *result;
    NSArray *views = [container subviews];
    for (UIImageView *img in views) {
        if (img.tag == value) {
            NSArray *containerViews = [img subviews];
            for (UIImageView *iconImg in containerViews) {
                result = iconImg;
            }
        }
    }
    return result;
}

- (void) buildClovesEven {
    CGFloat fanWidth = M_PI*2/IconsCount;
    CGFloat mid = 1.57;
    
    for (int i = 0; i < IconsCount; i++) {
        Clove *clove = [[Clove alloc] init];
        clove.midValue = mid;
        clove.minValue = mid - (fanWidth/2);
        clove.maxValue = mid + (fanWidth/2);
        clove.value = i;
        
        if (clove.maxValue-fanWidth < - M_PI) {
            mid = M_PI;
            clove.midValue = mid;
            clove.minValue = fabsf(clove.maxValue);
        }
        
        mid -= fanWidth;
        
        NSLog(@"Clove is %@", clove);
        
        [cloves addObject:clove];
    }
}

- (float) calculateDistanceFromCenter:(CGPoint)point {
    CGPoint center = CGPointMake(self.bounds.size.width/2.0f, self.bounds.size.height/2.0f);
    float dx = point.x - center.x;
    float dy = point.y - center.y;
    return sqrt(dx*dx + dy*dy);
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint touchPoint = [touch locationInView:self];
    float dist = [self calculateDistanceFromCenter:touchPoint];
    
    if (dist < 30 || dist > 200) {
        return NO;
    }
    
    float dx = touchPoint.x - container.center.x;
    float dy = touchPoint.y - container.center.y;
    deltaAngle = atan2(dy, dx);
    
    startTransform = container.transform;
    
    UIImageView *im = [self getCloveByValue:currentValue];
    im.alpha = minAlphavalue;
    im.image = [im.image imageTintedWithColor:[UIColor segmentOriginalColor]];

    return YES;
}

- (BOOL)continueTrackingWithTouch:(UITouch*)touch withEvent:(UIEvent*)event
{
    // Check if it is a tap or a drag start:
    continuedGesture = YES;
    
    CGPoint pt = [touch locationInView:self];
    
    float dist = [self calculateDistanceFromCenter:pt];
    
    if (dist < 30 || dist > 200)
    {
        NSLog(@"drag path too close to the center (%f,%f)", pt.x, pt.y);        
    }
    
    float dx = pt.x  - container.center.x;
    float dy = pt.y  - container.center.y;
    float ang = atan2(dy,dx);
    
    float angleDifference = deltaAngle - ang;
    
    container.transform = CGAffineTransformRotate(startTransform, -angleDifference);
    
    // ROTATE ICONS INSIDE THE CLOVES
//    for (int i = 0; i < IconsCount; i++) {
//        UIImageView *iconImg = [self getIconImageByValue:i];
//        iconImg.transform = CGAffineTransformRotate(startTransform, angleDifference);
//    }
    
    return YES;
}

- (void)endTrackingWithTouch:(UITouch*)touch withEvent:(UIEvent*)event
{
    if (!continuedGesture) {
        [self cloveClickEvent:[touch locationInView:self]];
    } else {
        continuedGesture = NO;
    }
    
    [self finishRotation];
}

- (void)rotateToPoint:(CGPoint)point {
    float dx = point.x - container.center.x;
    float dy = point.y - container.center.y;
    float ang = atan2(dy, dx);
    
    float angleDifference = topCloveAngle - ang;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    container.transform = CGAffineTransformRotate(startTransform, angleDifference - 90);
    
    [UIView commitAnimations];
}

- (void)finishRotation {
    CGFloat radians = atan2f(container.transform.b, container.transform.a);
    
    CGFloat newVal = 0.0;
    
    for (Clove *c in cloves) {
        if (c.minValue > 0 && c.maxValue < 0) { // anomalous case
            if (c.maxValue > radians || c.minValue < radians) {
                if (radians > 0) { // we are in the positive quadrant
                    newVal = radians - M_PI;
                } else { // we are in the negative one
                    newVal = M_PI + radians;
                }
                currentValue = c.value;
            }
        }
        else if (radians > c.minValue && radians < c.maxValue) {
            newVal = radians - c.midValue;
            currentValue = c.value;
        }
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.2];
    
    CGAffineTransform t = CGAffineTransformRotate(container.transform, -newVal);
    container.transform = t;
    
    [UIView commitAnimations];
    
    [self.delegate wheelDidChangeValue:[self getCloveName:currentValue]];
    
    UIImageView *im = [self getCloveByValue:currentValue];
    im.alpha = maxAlphavalue;
    im.image = [im.image imageTintedWithColor:[UIColor segmentHighlighedColor]];
}

//- (CGFloat) pointPairToBearingDegrees:(CGPoint)startingPoint secondPoint:(CGPoint) endingPoint
//{
//    CGPoint originPoint = CGPointMake(endingPoint.x - startingPoint.x, endingPoint.y - startingPoint.y); // get origin point to origin by subtracting end from start
//    float bearingRadians = atan2f(originPoint.y, originPoint.x); // get bearing in radians
//    float bearingDegrees = bearingRadians * (180.0 / M_PI); // convert to degrees
//    bearingDegrees = (bearingDegrees > 0.0 ? bearingDegrees : (360.0 + bearingDegrees)); // correct discontinuity
//    return bearingDegrees;
//}

- (void)cloveClickEvent:(CGPoint)point {
    int x = container.center.x;
    int y = container.center.y;
    [self rotateToPoint:point];
    NSLog(@"Container center: %i, %i", x, y);
}

- (NSString *) getCloveName:(int)position {
    NSString *res = @"";
    
    switch ((SegmentIcons)position) {
        case Age:
            res = @"Age demographic information...";
            break;
        case Car:
            res = @"Car type demographic information...";
            break;
        case Gender:
            res = @"Gender demographic information...";
            break;
        case Location:
            res = @"Local demographic information...";
            break;
        default:
            break;
    }
    return res;
}
@end
